﻿namespace Sistema.UI.Judicial
{
    partial class FExpedienteArbitrales
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions13 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject49 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject50 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject51 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject52 = new DevExpress.Utils.SerializableAppearanceObject();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FExpedienteArbitrales));
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions14 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject53 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject54 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject55 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject56 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions15 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject57 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject58 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject59 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject60 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions16 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject61 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject62 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject63 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject64 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions17 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject65 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject66 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject67 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject68 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.XtraEditors.Controls.EditorButtonImageOptions editorButtonImageOptions18 = new DevExpress.XtraEditors.Controls.EditorButtonImageOptions();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject69 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject70 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject71 = new DevExpress.Utils.SerializableAppearanceObject();
            DevExpress.Utils.SerializableAppearanceObject serializableAppearanceObject72 = new DevExpress.Utils.SerializableAppearanceObject();
            this.tpListaEdicion = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage1 = new DevExpress.XtraTab.XtraTabPage();
            this.lcLista = new DevExpress.XtraLayout.LayoutControl();
            this.btnMostrarArchivo = new DevExpress.XtraEditors.SimpleButton();
            this.btnAddE = new DevExpress.XtraEditors.SimpleButton();
            this.btnAddActoProcesal = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl2 = new DevExpress.XtraGrid.GridControl();
            this.bsActos = new System.Windows.Forms.BindingSource(this.components);
            this.gridView2 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colIdActoPro = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIdExpediente1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContenido = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colContenido1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIdNEWID1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIdExpedienteInstancia = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExpediente = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colActoProcesalContenido = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMostrar = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ribtnShow = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn5 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInstnacia = new DevExpress.XtraGrid.Columns.GridColumn();
            this.imgList = new System.Windows.Forms.ImageList(this.components);
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.bsLista = new System.Windows.Forms.BindingSource(this.components);
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colCodigo = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFechaInicio = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMontoSoles = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colArchivado = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNDemandados = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNDemandantes = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colMateriaArbitral = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colArchivadoSIoNO = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colModalidad = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colSedeArbitral = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDescripcionTipoArbitraje = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTipoMonedaControversia = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNombreAsesorLegalPatrocinador = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnMortrarArchivoGrid = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.splitterItem1 = new DevExpress.XtraLayout.SplitterItem();
            this.splitterItem2 = new DevExpress.XtraLayout.SplitterItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.dlcData = new ExtLayoutControl();
            this.glueSecretarioArbitral = new DevExpress.XtraEditors.GridLookUpEdit();
            this.bsEdicion = new System.Windows.Forms.BindingSource(this.components);
            this.gridView34 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.txtJustificacionContingencia = new DevExpress.XtraEditors.TextEdit();
            this.glueLaudoFavor = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView15 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.txtMateriaControvertida = new DevExpress.XtraEditors.TextEdit();
            this.glueIdSedeArbitral = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView14 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.txtMontoReconvencion = new DevExpress.XtraEditors.TextEdit();
            this.glueTipoMonedaLaudado = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridLookUpEdit3View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.txtMontoLaudado = new DevExpress.XtraEditors.TextEdit();
            this.glueTipoMonedaReconvencion = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView33 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.glueTipoMonedaCOntrovercia = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView30 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.txtTipoDocumento = new DevExpress.XtraEditors.TextEdit();
            this.LugarGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView31 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.textEdit3 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit2 = new DevExpress.XtraEditors.TextEdit();
            this.txtLugarProximaAudtextEdit1 = new DevExpress.XtraEditors.TextEdit();
            this.gridControl7 = new DevExpress.XtraGrid.GridControl();
            this.bsAsesorLegall = new System.Windows.Forms.BindingSource(this.components);
            this.gridView11 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIdAsesorLegalEstudio = new DevExpress.XtraGrid.Columns.GridColumn();
            this.rpiEstudioAbogados = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.gridView23 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colIdAsesorLegalAbogado = new DevExpress.XtraGrid.Columns.GridColumn();
            this.rpiAbogadoPorEstudio = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.repositoryItemGridLookUpEdit3View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colFechaInicio1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFechaFIn = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNroOS_RA_C = new DevExpress.XtraGrid.Columns.GridColumn();
            this.btnEditarActo = new DevExpress.XtraEditors.SimpleButton();
            this.btnAddActo = new DevExpress.XtraEditors.SimpleButton();
            this.btnEditInstancia = new DevExpress.XtraEditors.SimpleButton();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.gridControl6 = new DevExpress.XtraGrid.GridControl();
            this.bsInstanciasExpediente = new System.Windows.Forms.BindingSource(this.components);
            this.gridView27 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colIdInstancia2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIdOrgano2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFechaInicio3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colFechaFinal2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colAutomisorio2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colTasacionInstancia1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colApelacion2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colObservacion3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colComentarios2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInstanciaTexto = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colOrganoTexto = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.gridControl5 = new DevExpress.XtraGrid.GridControl();
            this.gridView25 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn10 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn11 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn12 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn13 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn15 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn16 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn17 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn18 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn19 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemButtonEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.gridColumn20 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn21 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn22 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn23 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colInstancia = new DevExpress.XtraGrid.Columns.GridColumn();
            this.glueTipoContingenciaGridLookUpEdit1 = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView29 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.GLueIdSupervisor = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridControl4 = new DevExpress.XtraGrid.GridControl();
            this.bsDocumentos = new System.Windows.Forms.BindingSource(this.components);
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.rpiglueTipoDoc = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.bsTipoDoc = new System.Windows.Forms.BindingSource(this.components);
            this.repositoryItemGridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colDescripcion1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumnCargarDoc = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ribtnCargarDOC = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.colGargarFolder = new DevExpress.XtraGrid.Columns.GridColumn();
            this.rbtnCargarFolder = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.gridColumnMostrarDOC = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ribtnShowDOC = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.colIdDocumento = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colNombre = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colExtension = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colIdNEWID3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colDocumento1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.colEliminar = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ribtnEliminarFile = new DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit();
            this.IdExpedienteTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.CodigoTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.glueUbicacion = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView24 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.glueModalidad = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView22 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.IdTipoProcesoGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridLookUpEdit2View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.deFechaProximaAudidateEdit1 = new DevExpress.XtraEditors.DateEdit();
            this.deFechaVEncimientoDateEdit1 = new DevExpress.XtraEditors.DateEdit();
            this.FechaInicioDateEdit = new DevExpress.XtraEditors.DateEdit();
            this.IdClaseProcesoGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView5 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.glueArchivadoGridLookUpEdit1 = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView28 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.IdAbogadoPatrocinanteGgridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView26 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.IdAbogadoGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView6 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.IdExpedientePadreGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView7 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.IdNEWIDTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.ActoJudicialGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView9 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.ActoProcesalGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView10 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.Expediente1GridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView12 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.Expediente2GridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView13 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.FechaMovimientoGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridLookUpEdit1View = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.NroInstanciaGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView8 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.ClaseProcesoGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView16 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.TipoProcesoGridLookUpEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView17 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.MontoSolesControversiaTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.MontoDolaresTextEdit = new DevExpress.XtraEditors.TextEdit();
            this.OrganoExpedienteDemandanteGridControl = new DevExpress.XtraGrid.GridControl();
            this.bsDemandante = new System.Windows.Forms.BindingSource(this.components);
            this.gridView18 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colIdDemandante1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.rpiglueDemandante = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.gridView20 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.OrganoExpedienteDemandadoGridControl = new DevExpress.XtraGrid.GridControl();
            this.bsDemandado = new System.Windows.Forms.BindingSource(this.components);
            this.gridView19 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.colIdDemandado1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.rpiglueDemandado = new DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit();
            this.gridView21 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.deHoraProximaAudiDateEdit2 = new DevExpress.XtraEditors.TimeEdit();
            this.glueCentroArbitral = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView32 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.EstadoActualgridLookUpEdit1 = new DevExpress.XtraEditors.TextEdit();
            this.glueContrato = new DevExpress.XtraEditors.TextEdit();
            this.IdNotificacionGgridLookUpEdit = new DevExpress.XtraEditors.TextEdit();
            this.DescripcionTextEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView35 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.ObservacionTextEdit = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView36 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.txtTercerArbitroAsignado = new DevExpress.XtraEditors.GridLookUpEdit();
            this.gridView37 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.deFechaInstalacionTribunal = new DevExpress.XtraEditors.TextEdit();
            this.ItemForIdExpediente = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForIdExpedientePadre = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForIdNEWID = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForActoJudicial = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForActoProcesal = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForExpediente1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForExpediente2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForFechaMovimiento = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForNroInstancia = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForClaseProceso = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForTipoProceso = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.lcgTabsEdicion = new DevExpress.XtraLayout.TabbedControlGroup();
            this.lcgEstadoArbitral = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem20 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForIdAbogado = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem41 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForMontoDolares = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem24 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.tabbedControlGroup2 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup8 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup9 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem21 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem22 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem23 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem27 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem26 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem28 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem7 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup10 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem34 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForMontoSoles = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup11 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem35 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem38 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup12 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem37 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem36 = new DevExpress.XtraLayout.LayoutControlItem();
            this.tabbedControlGroup1 = new DevExpress.XtraLayout.TabbedControlGroup();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem11 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem5 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlGroup7 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem13 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem14 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem4 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.lcgDatosGenerales = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForCodigo = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForFechaInicio = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem30 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForIdClaseProceso = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem32 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem31 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem25 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForOrganoExpedienteDemandante = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForOrganoExpedienteDemandado = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem39 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem6 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.lcgDatosArbitrajes = new DevExpress.XtraLayout.LayoutControlGroup();
            this.ItemForIdTipoProceso = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem42 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForObservacion = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem40 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem33 = new DevExpress.XtraLayout.LayoutControlItem();
            this.lycNrocasilla = new DevExpress.XtraLayout.LayoutControlItem();
            this.ItemForDescripcion = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem29 = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem8 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.splitterItem3 = new DevExpress.XtraLayout.SplitterItem();
            this.bsInstanciaConsulta = new System.Windows.Forms.BindingSource(this.components);
            this.bsInstancia = new System.Windows.Forms.BindingSource(this.components);
            this.bsOrganoJudicialEdit = new System.Windows.Forms.BindingSource(this.components);
            this.bsJuzgados = new System.Windows.Forms.BindingSource(this.components);
            this.popMenu = new DevExpress.XtraBars.PopupMenu(this.components);
            this.rbtnAddActoProce = new DevExpress.XtraBars.BarButtonItem();
            this.rbtnMostrarDatosAdjuntosExp = new DevExpress.XtraBars.BarButtonItem();
            this.popMenuActos = new DevExpress.XtraBars.PopupMenu(this.components);
            this.rbtnEditarActo = new DevExpress.XtraBars.BarButtonItem();
            this.rbtnVerAdjuntoActo = new DevExpress.XtraBars.BarButtonItem();
            this.ofdAll = new System.Windows.Forms.OpenFileDialog();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.rbtnFechaInicio = new DevExpress.XtraBars.BarEditItem();
            this.riFechaInicio = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.rbtnFechaFin = new DevExpress.XtraBars.BarEditItem();
            this.reFechaFin = new DevExpress.XtraEditors.Repository.RepositoryItemDateEdit();
            this.btnReporte = new DevExpress.XtraBars.BarButtonItem();
            this.printingSystem1 = new DevExpress.XtraPrinting.PrintingSystem(this.components);
            this.printableComponentLink1 = new DevExpress.XtraPrinting.PrintableComponentLink(this.components);
            this.folderDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.bsAbogado = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.RibbonForm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tpListaEdicion)).BeginInit();
            this.tpListaEdicion.SuspendLayout();
            this.xtraTabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.lcLista)).BeginInit();
            this.lcLista.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsActos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribtnShow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsLista)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnMortrarArchivoGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            this.xtraTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dlcData)).BeginInit();
            this.dlcData.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.glueSecretarioArbitral.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsEdicion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtJustificacionContingencia.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.glueLaudoFavor.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMateriaControvertida.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.glueIdSedeArbitral.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMontoReconvencion.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.glueTipoMonedaLaudado.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit3View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMontoLaudado.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.glueTipoMonedaReconvencion.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.glueTipoMonedaCOntrovercia.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTipoDocumento.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LugarGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit3.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLugarProximaAudtextEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsAsesorLegall)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rpiEstudioAbogados)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rpiAbogadoPorEstudio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit3View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsInstanciasExpediente)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.glueTipoContingenciaGridLookUpEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GLueIdSupervisor.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsDocumentos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rpiglueTipoDoc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsTipoDoc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribtnCargarDOC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbtnCargarFolder)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribtnShowDOC)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribtnEliminarFile)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdExpedienteTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CodigoTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.glueUbicacion.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.glueModalidad.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdTipoProcesoGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit2View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deFechaProximaAudidateEdit1.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deFechaProximaAudidateEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deFechaVEncimientoDateEdit1.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deFechaVEncimientoDateEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaInicioDateEdit.Properties.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaInicioDateEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdClaseProcesoGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.glueArchivadoGridLookUpEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdAbogadoPatrocinanteGgridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdAbogadoGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdExpedientePadreGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdNEWIDTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ActoJudicialGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ActoProcesalGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Expediente1GridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Expediente2GridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaMovimientoGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NroInstanciaGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClaseProcesoGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TipoProcesoGridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MontoSolesControversiaTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MontoDolaresTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.OrganoExpedienteDemandanteGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsDemandante)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rpiglueDemandante)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.OrganoExpedienteDemandadoGridControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsDemandado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rpiglueDemandado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deHoraProximaAudiDateEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.glueCentroArbitral.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.EstadoActualgridLookUpEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.glueContrato.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdNotificacionGgridLookUpEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DescripcionTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ObservacionTextEdit.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTercerArbitroAsignado.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.deFechaInstalacionTribunal.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIdExpediente)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIdExpedientePadre)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIdNEWID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForActoJudicial)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForActoProcesal)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForExpediente1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForExpediente2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFechaMovimiento)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNroInstancia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClaseProceso)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTipoProceso)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgTabsEdicion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgEstadoArbitral)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIdAbogado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForMontoDolares)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForMontoSoles)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgDatosGenerales)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCodigo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFechaInicio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIdClaseProceso)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOrganoExpedienteDemandante)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOrganoExpedienteDemandado)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgDatosArbitrajes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIdTipoProceso)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForObservacion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lycNrocasilla)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDescripcion)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsInstanciaConsulta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsInstancia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsOrganoJudicialEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsJuzgados)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popMenuActos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.riFechaInicio)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.riFechaInicio.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reFechaFin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.reFechaFin.CalendarTimeProperties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.printingSystem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsAbogado)).BeginInit();
            this.SuspendLayout();
            // 
            // RibbonForm
            // 
            this.RibbonForm.ColorScheme = DevExpress.XtraBars.Ribbon.RibbonControlColorScheme.Yellow;
            this.RibbonForm.ExpandCollapseItem.Id = 0;
            this.RibbonForm.Images = this.imgList;
            this.RibbonForm.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.rbtnAddActoProce,
            this.rbtnEditarActo,
            this.rbtnVerAdjuntoActo,
            this.rbtnMostrarDatosAdjuntosExp,
            this.rbtnFechaInicio,
            this.rbtnFechaFin,
            this.btnReporte});
            this.RibbonForm.Margin = new System.Windows.Forms.Padding(4, 7, 4, 7);
            this.RibbonForm.MaxItemId = 18;
            this.RibbonForm.OptionsMenuMinWidth = 577;
            this.RibbonForm.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.riFechaInicio,
            this.reFechaFin});
            // 
            // 
            // 
            this.RibbonForm.SearchEditItem.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Left;
            this.RibbonForm.SearchEditItem.EditWidth = 150;
            this.RibbonForm.SearchEditItem.Id = -5000;
            this.RibbonForm.SearchEditItem.ImageOptions.AllowGlyphSkinning = DevExpress.Utils.DefaultBoolean.True;
            this.RibbonForm.Size = new System.Drawing.Size(1624, 158);
            // 
            // GrupoFiltros
            // 
            this.GrupoFiltros.ItemLinks.Add(this.rbtnFechaInicio);
            this.GrupoFiltros.ItemLinks.Add(this.rbtnFechaFin);
            this.GrupoFiltros.ItemLinks.Add(this.btnReporte);
            // 
            // tpListaEdicion
            // 
            this.tpListaEdicion.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tpListaEdicion.Location = new System.Drawing.Point(0, 158);
            this.tpListaEdicion.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tpListaEdicion.Name = "tpListaEdicion";
            this.tpListaEdicion.SelectedTabPage = this.xtraTabPage1;
            this.tpListaEdicion.Size = new System.Drawing.Size(1624, 864);
            this.tpListaEdicion.TabIndex = 0;
            this.tpListaEdicion.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage1,
            this.xtraTabPage2});
            // 
            // xtraTabPage1
            // 
            this.xtraTabPage1.Controls.Add(this.lcLista);
            this.xtraTabPage1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.xtraTabPage1.Name = "xtraTabPage1";
            this.xtraTabPage1.Size = new System.Drawing.Size(1622, 817);
            this.xtraTabPage1.Text = "Lista";
            // 
            // lcLista
            // 
            this.lcLista.Controls.Add(this.btnMostrarArchivo);
            this.lcLista.Controls.Add(this.btnAddE);
            this.lcLista.Controls.Add(this.btnAddActoProcesal);
            this.lcLista.Controls.Add(this.gridControl2);
            this.lcLista.Controls.Add(this.gridControl1);
            this.lcLista.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lcLista.Location = new System.Drawing.Point(0, 0);
            this.lcLista.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.lcLista.Name = "lcLista";
            this.lcLista.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(370, 85, 250, 350);
            this.lcLista.Root = this.layoutControlGroup2;
            this.lcLista.Size = new System.Drawing.Size(1622, 817);
            this.lcLista.TabIndex = 1;
            this.lcLista.Text = "layoutControl1";
            // 
            // btnMostrarArchivo
            // 
            this.btnMostrarArchivo.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnMostrarArchivo.ImageOptions.Image = global::Sistema.UI.Properties.Resources.Search216x16;
            this.btnMostrarArchivo.Location = new System.Drawing.Point(899, 767);
            this.btnMostrarArchivo.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnMostrarArchivo.Name = "btnMostrarArchivo";
            this.btnMostrarArchivo.Size = new System.Drawing.Size(242, 32);
            this.btnMostrarArchivo.StyleController = this.lcLista;
            this.btnMostrarArchivo.TabIndex = 12;
            this.btnMostrarArchivo.Text = "Mostrar Archivo Adjunto";
            this.btnMostrarArchivo.Click += new System.EventHandler(this.btnMostrarArchivo_Click);
            // 
            // btnAddE
            // 
            this.btnAddE.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAddE.ImageOptions.Image = global::Sistema.UI.Properties.Resources.Edit16x16;
            this.btnAddE.Location = new System.Drawing.Point(680, 767);
            this.btnAddE.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnAddE.Name = "btnAddE";
            this.btnAddE.Size = new System.Drawing.Size(213, 32);
            this.btnAddE.StyleController = this.lcLista;
            this.btnAddE.TabIndex = 11;
            this.btnAddE.Text = "Editar Acto Procesal";
            this.btnAddE.Visible = false;
            this.btnAddE.Click += new System.EventHandler(this.btnAddE_Click);
            // 
            // btnAddActoProcesal
            // 
            this.btnAddActoProcesal.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAddActoProcesal.ImageOptions.Image = global::Sistema.UI.Properties.Resources.Add16x16;
            this.btnAddActoProcesal.Location = new System.Drawing.Point(442, 767);
            this.btnAddActoProcesal.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnAddActoProcesal.Name = "btnAddActoProcesal";
            this.btnAddActoProcesal.Size = new System.Drawing.Size(232, 32);
            this.btnAddActoProcesal.StyleController = this.lcLista;
            this.btnAddActoProcesal.TabIndex = 10;
            this.btnAddActoProcesal.Text = "Agregar Acto Procesal";
            this.btnAddActoProcesal.Visible = false;
            this.btnAddActoProcesal.Click += new System.EventHandler(this.btnAddActoProcesal_Click);
            // 
            // gridControl2
            // 
            this.gridControl2.DataSource = this.bsActos;
            this.gridControl2.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gridControl2.Location = new System.Drawing.Point(35, 517);
            this.gridControl2.MainView = this.gridView2;
            this.gridControl2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gridControl2.MenuManager = this.RibbonForm;
            this.gridControl2.Name = "gridControl2";
            this.gridControl2.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.ribtnShow});
            this.gridControl2.Size = new System.Drawing.Size(1543, 227);
            this.gridControl2.TabIndex = 5;
            this.gridControl2.Tag = "N";
            this.gridControl2.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView2});
            // 
            // bsActos
            // 
            this.bsActos.DataSource = typeof(Sistema.Model.ActoProcesalContenido);
            // 
            // gridView2
            // 
            this.gridView2.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colIdActoPro,
            this.colIdExpediente1,
            this.colContenido,
            this.colContenido1,
            this.colIdNEWID1,
            this.colIdExpedienteInstancia,
            this.colExpediente,
            this.colActoProcesalContenido,
            this.gridColumn1,
            this.gridColumn2,
            this.colMostrar,
            this.gridColumn4,
            this.gridColumn5,
            this.gridColumn8,
            this.gridColumn7,
            this.colInstnacia});
            this.gridView2.DetailHeight = 512;
            this.gridView2.GridControl = this.gridControl2;
            this.gridView2.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always;
            this.gridView2.Images = this.imgList;
            this.gridView2.Name = "gridView2";
            this.gridView2.OptionsBehavior.Editable = false;
            this.gridView2.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView2.OptionsView.ColumnAutoWidth = false;
            this.gridView2.OptionsView.ShowGroupPanel = false;
            this.gridView2.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn5, DevExpress.Data.ColumnSortOrder.Descending)});
            this.gridView2.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.gridView2_PopupMenuShowing);
            // 
            // colIdActoPro
            // 
            this.colIdActoPro.FieldName = "IdActoPro";
            this.colIdActoPro.MinWidth = 30;
            this.colIdActoPro.Name = "colIdActoPro";
            this.colIdActoPro.Width = 112;
            // 
            // colIdExpediente1
            // 
            this.colIdExpediente1.FieldName = "IdExpediente";
            this.colIdExpediente1.MinWidth = 30;
            this.colIdExpediente1.Name = "colIdExpediente1";
            this.colIdExpediente1.Width = 112;
            // 
            // colContenido
            // 
            this.colContenido.FieldName = "ActoProcesal.Contenido";
            this.colContenido.MinWidth = 30;
            this.colContenido.Name = "colContenido";
            this.colContenido.Visible = true;
            this.colContenido.VisibleIndex = 4;
            this.colContenido.Width = 502;
            // 
            // colContenido1
            // 
            this.colContenido1.FieldName = "Contenido1";
            this.colContenido1.MinWidth = 30;
            this.colContenido1.Name = "colContenido1";
            this.colContenido1.Width = 453;
            // 
            // colIdNEWID1
            // 
            this.colIdNEWID1.FieldName = "IdNEWID";
            this.colIdNEWID1.MinWidth = 30;
            this.colIdNEWID1.Name = "colIdNEWID1";
            this.colIdNEWID1.Width = 112;
            // 
            // colIdExpedienteInstancia
            // 
            this.colIdExpedienteInstancia.FieldName = "IdExpedienteInstancia";
            this.colIdExpedienteInstancia.MinWidth = 30;
            this.colIdExpedienteInstancia.Name = "colIdExpedienteInstancia";
            this.colIdExpedienteInstancia.Width = 112;
            // 
            // colExpediente
            // 
            this.colExpediente.FieldName = "Expediente";
            this.colExpediente.MinWidth = 30;
            this.colExpediente.Name = "colExpediente";
            this.colExpediente.Width = 112;
            // 
            // colActoProcesalContenido
            // 
            this.colActoProcesalContenido.FieldName = "ActoProcesalContenido";
            this.colActoProcesalContenido.MinWidth = 30;
            this.colActoProcesalContenido.Name = "colActoProcesalContenido";
            this.colActoProcesalContenido.Width = 112;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Tipo Contenido ";
            this.gridColumn1.FieldName = "Expediente";
            this.gridColumn1.MinWidth = 30;
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Width = 487;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Tipo Contenido";
            this.gridColumn2.FieldName = "TipoContenido.Descripcion";
            this.gridColumn2.MinWidth = 30;
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 5;
            this.gridColumn2.Width = 196;
            // 
            // colMostrar
            // 
            this.colMostrar.Caption = "Mostrar";
            this.colMostrar.ColumnEdit = this.ribtnShow;
            this.colMostrar.ImageOptions.ImageIndex = 3;
            this.colMostrar.MinWidth = 30;
            this.colMostrar.Name = "colMostrar";
            this.colMostrar.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.colMostrar.OptionsColumn.ShowCaption = false;
            this.colMostrar.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.colMostrar.Width = 51;
            // 
            // ribtnShow
            // 
            this.ribtnShow.AutoHeight = false;
            editorButtonImageOptions13.Image = global::Sistema.UI.Properties.Resources.Search16x16;
            this.ribtnShow.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, editorButtonImageOptions13, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject49, serializableAppearanceObject50, serializableAppearanceObject51, serializableAppearanceObject52, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.ribtnShow.Name = "ribtnShow";
            this.ribtnShow.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.ribtnShow_ButtonClick);
            this.ribtnShow.Click += new System.EventHandler(this.ribtnShow_Click);
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "Resolución";
            this.gridColumn4.FieldName = "ActoProcesal.Codigo";
            this.gridColumn4.MinWidth = 30;
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 0;
            this.gridColumn4.Width = 162;
            // 
            // gridColumn5
            // 
            this.gridColumn5.Caption = "Fecha";
            this.gridColumn5.FieldName = "ActoProcesal.FechaRegistro";
            this.gridColumn5.MinWidth = 30;
            this.gridColumn5.Name = "gridColumn5";
            this.gridColumn5.Visible = true;
            this.gridColumn5.VisibleIndex = 1;
            this.gridColumn5.Width = 100;
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "Recepción";
            this.gridColumn8.FieldName = "ActoProcesal.FechaRecepcion";
            this.gridColumn8.MinWidth = 30;
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 2;
            this.gridColumn8.Width = 103;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "Fecha Acto";
            this.gridColumn7.FieldName = "ActoProcesal.FechaAvisoAlerta";
            this.gridColumn7.MinWidth = 30;
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 3;
            this.gridColumn7.Width = 103;
            // 
            // colInstnacia
            // 
            this.colInstnacia.Caption = "Instancia";
            this.colInstnacia.FieldName = "ActoProcesal.DescripcionInstancia";
            this.colInstnacia.MinWidth = 30;
            this.colInstnacia.Name = "colInstnacia";
            this.colInstnacia.Width = 112;
            // 
            // imgList
            // 
            this.imgList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imgList.ImageStream")));
            this.imgList.TransparentColor = System.Drawing.Color.Magenta;
            this.imgList.Images.SetKeyName(0, "");
            this.imgList.Images.SetKeyName(1, "");
            this.imgList.Images.SetKeyName(2, "");
            this.imgList.Images.SetKeyName(3, "Search216x16.png");
            this.imgList.Images.SetKeyName(4, "Attach16x16.png");
            this.imgList.Images.SetKeyName(5, "Add16x16.png");
            // 
            // gridControl1
            // 
            this.gridControl1.DataSource = this.bsLista;
            this.gridControl1.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gridControl1.Location = new System.Drawing.Point(18, 18);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gridControl1.MenuManager = this.RibbonForm;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.btnMortrarArchivoGrid});
            this.gridControl1.Size = new System.Drawing.Size(1577, 438);
            this.gridControl1.TabIndex = 0;
            this.gridControl1.Tag = "N";
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            this.gridControl1.DoubleClick += new System.EventHandler(this.gridControl1_DoubleClick);
            // 
            // bsLista
            // 
            this.bsLista.DataSource = typeof(Sistema.Model.viewExpediente);
            this.bsLista.CurrentChanged += new System.EventHandler(this.bsLista_CurrentChanged);
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colCodigo,
            this.colFechaInicio,
            this.colMontoSoles,
            this.colArchivado,
            this.colNDemandados,
            this.colNDemandantes,
            this.colMateriaArbitral,
            this.colArchivadoSIoNO,
            this.colModalidad,
            this.colSedeArbitral,
            this.colDescripcionTipoArbitraje,
            this.colTipoMonedaControversia,
            this.colNombreAsesorLegalPatrocinador});
            this.gridView1.DetailHeight = 512;
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always;
            this.gridView1.Images = this.imgList;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.Editable = false;
            this.gridView1.OptionsFind.AlwaysVisible = true;
            this.gridView1.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView1.OptionsView.ColumnAutoWidth = false;
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colFechaInicio, DevExpress.Data.ColumnSortOrder.Ascending)});
            this.gridView1.Tag = "NB";
            this.gridView1.VertScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always;
            this.gridView1.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.gridView1_PopupMenuShowing);
            // 
            // colCodigo
            // 
            this.colCodigo.Caption = "Nro. de Expediente";
            this.colCodigo.FieldName = "Codigo";
            this.colCodigo.MinWidth = 30;
            this.colCodigo.Name = "colCodigo";
            this.colCodigo.OptionsColumn.ReadOnly = true;
            this.colCodigo.Visible = true;
            this.colCodigo.VisibleIndex = 0;
            this.colCodigo.Width = 159;
            // 
            // colFechaInicio
            // 
            this.colFechaInicio.Caption = "Fecha";
            this.colFechaInicio.FieldName = "FechaInicio";
            this.colFechaInicio.MinWidth = 30;
            this.colFechaInicio.Name = "colFechaInicio";
            this.colFechaInicio.OptionsColumn.ReadOnly = true;
            this.colFechaInicio.Visible = true;
            this.colFechaInicio.VisibleIndex = 1;
            this.colFechaInicio.Width = 157;
            // 
            // colMontoSoles
            // 
            this.colMontoSoles.Caption = "Monto";
            this.colMontoSoles.FieldName = "MontoSoles";
            this.colMontoSoles.MinWidth = 30;
            this.colMontoSoles.Name = "colMontoSoles";
            this.colMontoSoles.OptionsColumn.ReadOnly = true;
            this.colMontoSoles.Visible = true;
            this.colMontoSoles.VisibleIndex = 10;
            this.colMontoSoles.Width = 132;
            // 
            // colArchivado
            // 
            this.colArchivado.FieldName = "Archivado";
            this.colArchivado.MinWidth = 31;
            this.colArchivado.Name = "colArchivado";
            this.colArchivado.Width = 139;
            // 
            // colNDemandados
            // 
            this.colNDemandados.Caption = "Demandados";
            this.colNDemandados.FieldName = "NDemandados";
            this.colNDemandados.MinWidth = 30;
            this.colNDemandados.Name = "colNDemandados";
            this.colNDemandados.Visible = true;
            this.colNDemandados.VisibleIndex = 7;
            this.colNDemandados.Width = 301;
            // 
            // colNDemandantes
            // 
            this.colNDemandantes.Caption = "Demandantes";
            this.colNDemandantes.FieldName = "NDemandantes";
            this.colNDemandantes.MinWidth = 30;
            this.colNDemandantes.Name = "colNDemandantes";
            this.colNDemandantes.Visible = true;
            this.colNDemandantes.VisibleIndex = 6;
            this.colNDemandantes.Width = 351;
            // 
            // colMateriaArbitral
            // 
            this.colMateriaArbitral.Caption = "Materia";
            this.colMateriaArbitral.FieldName = "MateriaArbitral";
            this.colMateriaArbitral.MinWidth = 30;
            this.colMateriaArbitral.Name = "colMateriaArbitral";
            this.colMateriaArbitral.Visible = true;
            this.colMateriaArbitral.VisibleIndex = 4;
            this.colMateriaArbitral.Width = 154;
            // 
            // colArchivadoSIoNO
            // 
            this.colArchivadoSIoNO.Caption = "Archivado";
            this.colArchivadoSIoNO.FieldName = "ArchivadoSIoNO";
            this.colArchivadoSIoNO.MinWidth = 31;
            this.colArchivadoSIoNO.Name = "colArchivadoSIoNO";
            this.colArchivadoSIoNO.Visible = true;
            this.colArchivadoSIoNO.VisibleIndex = 11;
            this.colArchivadoSIoNO.Width = 139;
            // 
            // colModalidad
            // 
            this.colModalidad.Caption = "Modalidad";
            this.colModalidad.FieldName = "Modalidad";
            this.colModalidad.MinWidth = 30;
            this.colModalidad.Name = "colModalidad";
            this.colModalidad.Visible = true;
            this.colModalidad.VisibleIndex = 5;
            this.colModalidad.Width = 226;
            // 
            // colSedeArbitral
            // 
            this.colSedeArbitral.Caption = "Sede Arbitral";
            this.colSedeArbitral.FieldName = "SedeArbitral";
            this.colSedeArbitral.MinWidth = 30;
            this.colSedeArbitral.Name = "colSedeArbitral";
            this.colSedeArbitral.Visible = true;
            this.colSedeArbitral.VisibleIndex = 2;
            this.colSedeArbitral.Width = 175;
            // 
            // colDescripcionTipoArbitraje
            // 
            this.colDescripcionTipoArbitraje.Caption = "Tipo Arbitraje";
            this.colDescripcionTipoArbitraje.FieldName = "DescripcionTipoArbitraje";
            this.colDescripcionTipoArbitraje.MinWidth = 30;
            this.colDescripcionTipoArbitraje.Name = "colDescripcionTipoArbitraje";
            this.colDescripcionTipoArbitraje.Visible = true;
            this.colDescripcionTipoArbitraje.VisibleIndex = 3;
            this.colDescripcionTipoArbitraje.Width = 195;
            // 
            // colTipoMonedaControversia
            // 
            this.colTipoMonedaControversia.Caption = "Moneda";
            this.colTipoMonedaControversia.FieldName = "TipoMonedaControversia";
            this.colTipoMonedaControversia.MinWidth = 30;
            this.colTipoMonedaControversia.Name = "colTipoMonedaControversia";
            this.colTipoMonedaControversia.Visible = true;
            this.colTipoMonedaControversia.VisibleIndex = 9;
            this.colTipoMonedaControversia.Width = 112;
            // 
            // colNombreAsesorLegalPatrocinador
            // 
            this.colNombreAsesorLegalPatrocinador.Caption = "Patrocinante";
            this.colNombreAsesorLegalPatrocinador.FieldName = "NombreAsesorLegalPatrocinador";
            this.colNombreAsesorLegalPatrocinador.MinWidth = 30;
            this.colNombreAsesorLegalPatrocinador.Name = "colNombreAsesorLegalPatrocinador";
            this.colNombreAsesorLegalPatrocinador.Visible = true;
            this.colNombreAsesorLegalPatrocinador.VisibleIndex = 8;
            this.colNombreAsesorLegalPatrocinador.Width = 319;
            // 
            // btnMortrarArchivoGrid
            // 
            this.btnMortrarArchivoGrid.AutoHeight = false;
            editorButtonImageOptions14.Image = global::Sistema.UI.Properties.Resources.Search16x16;
            this.btnMortrarArchivoGrid.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, editorButtonImageOptions14, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject53, serializableAppearanceObject54, serializableAppearanceObject55, serializableAppearanceObject56, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.btnMortrarArchivoGrid.Name = "btnMortrarArchivoGrid";
            this.btnMortrarArchivoGrid.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.btnMortrarArchivoGrid_ButtonClick);
            this.btnMortrarArchivoGrid.ButtonPressed += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.btnMortrarArchivoGrid_ButtonPressed);
            this.btnMortrarArchivoGrid.Click += new System.EventHandler(this.btnMortrarArchivoGrid_Click);
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "layoutControlGroup2";
            this.layoutControlGroup2.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.splitterItem1,
            this.splitterItem2,
            this.layoutControlItem4,
            this.layoutControlItem8,
            this.layoutControlItem7,
            this.layoutControlGroup5,
            this.emptySpaceItem1,
            this.emptySpaceItem2});
            this.layoutControlGroup2.Name = "Root";
            this.layoutControlGroup2.Size = new System.Drawing.Size(1622, 817);
            this.layoutControlGroup2.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.gridControl1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(1583, 444);
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextVisible = false;
            // 
            // splitterItem1
            // 
            this.splitterItem1.AllowHotTrack = true;
            this.splitterItem1.CustomizationFormText = "splitterItem1";
            this.splitterItem1.Location = new System.Drawing.Point(0, 444);
            this.splitterItem1.Name = "splitterItem1";
            this.splitterItem1.Size = new System.Drawing.Size(1583, 9);
            // 
            // splitterItem2
            // 
            this.splitterItem2.AllowHotTrack = true;
            this.splitterItem2.CustomizationFormText = "splitterItem2";
            this.splitterItem2.Location = new System.Drawing.Point(1583, 0);
            this.splitterItem2.Name = "splitterItem2";
            this.splitterItem2.Size = new System.Drawing.Size(9, 749);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.btnMostrarArchivo;
            this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem4.Location = new System.Drawing.Point(881, 749);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(248, 38);
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextVisible = false;
            this.layoutControlItem4.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.btnAddE;
            this.layoutControlItem8.CustomizationFormText = "layoutControlItem8";
            this.layoutControlItem8.Location = new System.Drawing.Point(662, 749);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(219, 38);
            this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem8.TextVisible = false;
            this.layoutControlItem8.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.btnAddActoProcesal;
            this.layoutControlItem7.CustomizationFormText = "layoutControlItem7";
            this.layoutControlItem7.Location = new System.Drawing.Point(424, 749);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(238, 38);
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextVisible = false;
            this.layoutControlItem7.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.CustomizationFormText = "ACTO PROCESAL";
            this.layoutControlGroup5.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem3});
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 453);
            this.layoutControlGroup5.Name = "layoutControlGroup5";
            this.layoutControlGroup5.Size = new System.Drawing.Size(1583, 296);
            this.layoutControlGroup5.Text = "ACTOS PROCESALES";
            this.layoutControlGroup5.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.gridControl2;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(1549, 233);
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(0, 749);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(424, 38);
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(1129, 749);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(463, 38);
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Controls.Add(this.dlcData);
            this.xtraTabPage2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(1622, 817);
            this.xtraTabPage2.Text = "Editar";
            // 
            // dlcData
            // 
            this.dlcData.Controls.Add(this.glueSecretarioArbitral);
            this.dlcData.Controls.Add(this.txtJustificacionContingencia);
            this.dlcData.Controls.Add(this.glueLaudoFavor);
            this.dlcData.Controls.Add(this.txtMateriaControvertida);
            this.dlcData.Controls.Add(this.glueIdSedeArbitral);
            this.dlcData.Controls.Add(this.txtMontoReconvencion);
            this.dlcData.Controls.Add(this.glueTipoMonedaLaudado);
            this.dlcData.Controls.Add(this.txtMontoLaudado);
            this.dlcData.Controls.Add(this.glueTipoMonedaReconvencion);
            this.dlcData.Controls.Add(this.glueTipoMonedaCOntrovercia);
            this.dlcData.Controls.Add(this.txtTipoDocumento);
            this.dlcData.Controls.Add(this.LugarGridLookUpEdit);
            this.dlcData.Controls.Add(this.textEdit3);
            this.dlcData.Controls.Add(this.textEdit2);
            this.dlcData.Controls.Add(this.txtLugarProximaAudtextEdit1);
            this.dlcData.Controls.Add(this.gridControl7);
            this.dlcData.Controls.Add(this.btnEditarActo);
            this.dlcData.Controls.Add(this.btnAddActo);
            this.dlcData.Controls.Add(this.btnEditInstancia);
            this.dlcData.Controls.Add(this.simpleButton1);
            this.dlcData.Controls.Add(this.gridControl6);
            this.dlcData.Controls.Add(this.gridControl5);
            this.dlcData.Controls.Add(this.glueTipoContingenciaGridLookUpEdit1);
            this.dlcData.Controls.Add(this.GLueIdSupervisor);
            this.dlcData.Controls.Add(this.gridControl4);
            this.dlcData.Controls.Add(this.IdExpedienteTextEdit);
            this.dlcData.Controls.Add(this.CodigoTextEdit);
            this.dlcData.Controls.Add(this.glueUbicacion);
            this.dlcData.Controls.Add(this.glueModalidad);
            this.dlcData.Controls.Add(this.IdTipoProcesoGridLookUpEdit);
            this.dlcData.Controls.Add(this.deFechaProximaAudidateEdit1);
            this.dlcData.Controls.Add(this.deFechaVEncimientoDateEdit1);
            this.dlcData.Controls.Add(this.FechaInicioDateEdit);
            this.dlcData.Controls.Add(this.IdClaseProcesoGridLookUpEdit);
            this.dlcData.Controls.Add(this.glueArchivadoGridLookUpEdit1);
            this.dlcData.Controls.Add(this.IdAbogadoPatrocinanteGgridLookUpEdit);
            this.dlcData.Controls.Add(this.IdAbogadoGridLookUpEdit);
            this.dlcData.Controls.Add(this.IdExpedientePadreGridLookUpEdit);
            this.dlcData.Controls.Add(this.IdNEWIDTextEdit);
            this.dlcData.Controls.Add(this.ActoJudicialGridLookUpEdit);
            this.dlcData.Controls.Add(this.ActoProcesalGridLookUpEdit);
            this.dlcData.Controls.Add(this.Expediente1GridLookUpEdit);
            this.dlcData.Controls.Add(this.Expediente2GridLookUpEdit);
            this.dlcData.Controls.Add(this.FechaMovimientoGridLookUpEdit);
            this.dlcData.Controls.Add(this.NroInstanciaGridLookUpEdit);
            this.dlcData.Controls.Add(this.ClaseProcesoGridLookUpEdit);
            this.dlcData.Controls.Add(this.TipoProcesoGridLookUpEdit);
            this.dlcData.Controls.Add(this.MontoSolesControversiaTextEdit);
            this.dlcData.Controls.Add(this.MontoDolaresTextEdit);
            this.dlcData.Controls.Add(this.OrganoExpedienteDemandanteGridControl);
            this.dlcData.Controls.Add(this.OrganoExpedienteDemandadoGridControl);
            this.dlcData.Controls.Add(this.deHoraProximaAudiDateEdit2);
            this.dlcData.Controls.Add(this.glueCentroArbitral);
            this.dlcData.Controls.Add(this.EstadoActualgridLookUpEdit1);
            this.dlcData.Controls.Add(this.glueContrato);
            this.dlcData.Controls.Add(this.IdNotificacionGgridLookUpEdit);
            this.dlcData.Controls.Add(this.DescripcionTextEdit);
            this.dlcData.Controls.Add(this.ObservacionTextEdit);
            this.dlcData.Controls.Add(this.txtTercerArbitroAsignado);
            this.dlcData.Controls.Add(this.deFechaInstalacionTribunal);
            this.dlcData.DataSource = this.bsEdicion;
            this.dlcData.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForIdExpediente,
            this.ItemForIdExpedientePadre,
            this.ItemForIdNEWID,
            this.ItemForActoJudicial,
            this.ItemForActoProcesal,
            this.ItemForExpediente1,
            this.ItemForExpediente2,
            this.ItemForFechaMovimiento,
            this.ItemForNroInstancia,
            this.ItemForClaseProceso,
            this.ItemForTipoProceso});
            this.dlcData.Location = new System.Drawing.Point(2, 1);
            this.dlcData.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.dlcData.Name = "dlcData";
            this.dlcData.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1002, 157, 471, 595);
            this.dlcData.Root = this.layoutControlGroup1;
            this.dlcData.Size = new System.Drawing.Size(1626, 1101);
            this.dlcData.TabIndex = 0;
            this.dlcData.Text = "extLayoutControl1";
            // 
            // glueSecretarioArbitral
            // 
            this.glueSecretarioArbitral.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bsEdicion, "SecretarioArbitral", true));
            this.glueSecretarioArbitral.Location = new System.Drawing.Point(226, 202);
            this.glueSecretarioArbitral.MenuManager = this.RibbonForm;
            this.glueSecretarioArbitral.Name = "glueSecretarioArbitral";
            this.glueSecretarioArbitral.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.glueSecretarioArbitral.Properties.PopupView = this.gridView34;
            this.glueSecretarioArbitral.Size = new System.Drawing.Size(1365, 26);
            this.glueSecretarioArbitral.StyleController = this.dlcData;
            this.glueSecretarioArbitral.TabIndex = 66;
            this.glueSecretarioArbitral.Tag = "N";
            // 
            // bsEdicion
            // 
            this.bsEdicion.DataSource = typeof(Sistema.Model.Expediente);
            // 
            // gridView34
            // 
            this.gridView34.DetailHeight = 349;
            this.gridView34.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView34.Name = "gridView34";
            this.gridView34.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView34.OptionsView.ShowGroupPanel = false;
            // 
            // txtJustificacionContingencia
            // 
            this.txtJustificacionContingencia.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bsEdicion, "JustificacionContingencia", true));
            this.txtJustificacionContingencia.Location = new System.Drawing.Point(226, 106);
            this.txtJustificacionContingencia.MenuManager = this.RibbonForm;
            this.txtJustificacionContingencia.Name = "txtJustificacionContingencia";
            this.txtJustificacionContingencia.Size = new System.Drawing.Size(584, 26);
            this.txtJustificacionContingencia.StyleController = this.dlcData;
            this.txtJustificacionContingencia.TabIndex = 65;
            // 
            // glueLaudoFavor
            // 
            this.glueLaudoFavor.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bsEdicion, "LaudoFavor", true));
            this.glueLaudoFavor.Location = new System.Drawing.Point(226, 330);
            this.glueLaudoFavor.MenuManager = this.RibbonForm;
            this.glueLaudoFavor.Name = "glueLaudoFavor";
            this.glueLaudoFavor.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.glueLaudoFavor.Properties.PopupView = this.gridView15;
            this.glueLaudoFavor.Size = new System.Drawing.Size(1365, 26);
            this.glueLaudoFavor.StyleController = this.dlcData;
            this.glueLaudoFavor.TabIndex = 64;
            // 
            // gridView15
            // 
            this.gridView15.DetailHeight = 349;
            this.gridView15.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView15.Name = "gridView15";
            this.gridView15.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView15.OptionsView.ShowGroupPanel = false;
            // 
            // txtMateriaControvertida
            // 
            this.txtMateriaControvertida.Location = new System.Drawing.Point(226, 234);
            this.txtMateriaControvertida.MenuManager = this.RibbonForm;
            this.txtMateriaControvertida.Name = "txtMateriaControvertida";
            this.txtMateriaControvertida.Size = new System.Drawing.Size(1365, 26);
            this.txtMateriaControvertida.StyleController = this.dlcData;
            this.txtMateriaControvertida.TabIndex = 63;
            this.txtMateriaControvertida.Tag = "";
            // 
            // glueIdSedeArbitral
            // 
            this.glueIdSedeArbitral.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bsEdicion, "IdSedeArbitral", true));
            this.glueIdSedeArbitral.Location = new System.Drawing.Point(226, 74);
            this.glueIdSedeArbitral.MenuManager = this.RibbonForm;
            this.glueIdSedeArbitral.Name = "glueIdSedeArbitral";
            this.glueIdSedeArbitral.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.glueIdSedeArbitral.Properties.PopupView = this.gridView14;
            this.glueIdSedeArbitral.Size = new System.Drawing.Size(1365, 26);
            this.glueIdSedeArbitral.StyleController = this.dlcData;
            this.glueIdSedeArbitral.TabIndex = 62;
            this.glueIdSedeArbitral.Tag = "N";
            // 
            // gridView14
            // 
            this.gridView14.DetailHeight = 349;
            this.gridView14.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView14.Name = "gridView14";
            this.gridView14.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView14.OptionsView.ShowGroupPanel = false;
            // 
            // txtMontoReconvencion
            // 
            this.txtMontoReconvencion.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bsEdicion, "MontoReconvencion", true));
            this.txtMontoReconvencion.Location = new System.Drawing.Point(863, 216);
            this.txtMontoReconvencion.MenuManager = this.RibbonForm;
            this.txtMontoReconvencion.Name = "txtMontoReconvencion";
            this.txtMontoReconvencion.Size = new System.Drawing.Size(188, 26);
            this.txtMontoReconvencion.StyleController = this.dlcData;
            this.txtMontoReconvencion.TabIndex = 61;
            // 
            // glueTipoMonedaLaudado
            // 
            this.glueTipoMonedaLaudado.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bsEdicion, "TipoMonedaLaudado", true));
            this.glueTipoMonedaLaudado.Location = new System.Drawing.Point(1188, 216);
            this.glueTipoMonedaLaudado.MenuManager = this.RibbonForm;
            this.glueTipoMonedaLaudado.Name = "glueTipoMonedaLaudado";
            this.glueTipoMonedaLaudado.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.glueTipoMonedaLaudado.Properties.PopupView = this.gridLookUpEdit3View;
            this.glueTipoMonedaLaudado.Size = new System.Drawing.Size(141, 26);
            this.glueTipoMonedaLaudado.StyleController = this.dlcData;
            this.glueTipoMonedaLaudado.TabIndex = 60;
            // 
            // gridLookUpEdit3View
            // 
            this.gridLookUpEdit3View.DetailHeight = 349;
            this.gridLookUpEdit3View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridLookUpEdit3View.Name = "gridLookUpEdit3View";
            this.gridLookUpEdit3View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit3View.OptionsView.ShowGroupPanel = false;
            // 
            // txtMontoLaudado
            // 
            this.txtMontoLaudado.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bsEdicion, "MontoLaudado", true));
            this.txtMontoLaudado.Location = new System.Drawing.Point(1384, 216);
            this.txtMontoLaudado.MenuManager = this.RibbonForm;
            this.txtMontoLaudado.Name = "txtMontoLaudado";
            this.txtMontoLaudado.Size = new System.Drawing.Size(190, 26);
            this.txtMontoLaudado.StyleController = this.dlcData;
            this.txtMontoLaudado.TabIndex = 59;
            // 
            // glueTipoMonedaReconvencion
            // 
            this.glueTipoMonedaReconvencion.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bsEdicion, "TipoMonedaReconvencion", true));
            this.glueTipoMonedaReconvencion.Location = new System.Drawing.Point(669, 216);
            this.glueTipoMonedaReconvencion.MenuManager = this.RibbonForm;
            this.glueTipoMonedaReconvencion.Name = "glueTipoMonedaReconvencion";
            this.glueTipoMonedaReconvencion.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.glueTipoMonedaReconvencion.Properties.PopupView = this.gridView33;
            this.glueTipoMonedaReconvencion.Size = new System.Drawing.Size(139, 26);
            this.glueTipoMonedaReconvencion.StyleController = this.dlcData;
            this.glueTipoMonedaReconvencion.TabIndex = 58;
            // 
            // gridView33
            // 
            this.gridView33.DetailHeight = 349;
            this.gridView33.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView33.Name = "gridView33";
            this.gridView33.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView33.OptionsView.ShowGroupPanel = false;
            // 
            // glueTipoMonedaCOntrovercia
            // 
            this.glueTipoMonedaCOntrovercia.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bsEdicion, "TipoMonedaControversia", true));
            this.glueTipoMonedaCOntrovercia.Location = new System.Drawing.Point(149, 216);
            this.glueTipoMonedaCOntrovercia.MenuManager = this.RibbonForm;
            this.glueTipoMonedaCOntrovercia.Name = "glueTipoMonedaCOntrovercia";
            this.glueTipoMonedaCOntrovercia.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.glueTipoMonedaCOntrovercia.Properties.PopupView = this.gridView30;
            this.glueTipoMonedaCOntrovercia.Size = new System.Drawing.Size(140, 26);
            this.glueTipoMonedaCOntrovercia.StyleController = this.dlcData;
            this.glueTipoMonedaCOntrovercia.TabIndex = 57;
            // 
            // gridView30
            // 
            this.gridView30.DetailHeight = 349;
            this.gridView30.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView30.Name = "gridView30";
            this.gridView30.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView30.OptionsView.ShowGroupPanel = false;
            // 
            // txtTipoDocumento
            // 
            this.txtTipoDocumento.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bsEdicion, "TipoDocumento", true));
            this.txtTipoDocumento.Location = new System.Drawing.Point(226, 106);
            this.txtTipoDocumento.MenuManager = this.RibbonForm;
            this.txtTipoDocumento.Name = "txtTipoDocumento";
            this.txtTipoDocumento.Size = new System.Drawing.Size(584, 26);
            this.txtTipoDocumento.StyleController = this.dlcData;
            this.txtTipoDocumento.TabIndex = 55;
            this.txtTipoDocumento.Tag = "N";
            // 
            // LugarGridLookUpEdit
            // 
            this.LugarGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bsEdicion, "LugarProximaAudiencia", true));
            this.LugarGridLookUpEdit.Location = new System.Drawing.Point(243, 449);
            this.LugarGridLookUpEdit.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.LugarGridLookUpEdit.MenuManager = this.RibbonForm;
            this.LugarGridLookUpEdit.Name = "LugarGridLookUpEdit";
            this.LugarGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.LugarGridLookUpEdit.Properties.PopupView = this.gridView31;
            this.LugarGridLookUpEdit.Size = new System.Drawing.Size(1331, 26);
            this.LugarGridLookUpEdit.StyleController = this.dlcData;
            this.LugarGridLookUpEdit.TabIndex = 7;
            // 
            // gridView31
            // 
            this.gridView31.DetailHeight = 512;
            this.gridView31.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView31.Name = "gridView31";
            this.gridView31.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView31.OptionsView.ShowGroupPanel = false;
            // 
            // textEdit3
            // 
            this.textEdit3.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bsEdicion, "NroDiasNotificacion", true));
            this.textEdit3.Location = new System.Drawing.Point(243, 513);
            this.textEdit3.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textEdit3.MenuManager = this.RibbonForm;
            this.textEdit3.Name = "textEdit3";
            this.textEdit3.Size = new System.Drawing.Size(1331, 26);
            this.textEdit3.StyleController = this.dlcData;
            this.textEdit3.TabIndex = 53;
            // 
            // textEdit2
            // 
            this.textEdit2.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bsEdicion, "TipoNotificacionMail", true));
            this.textEdit2.Location = new System.Drawing.Point(243, 353);
            this.textEdit2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.textEdit2.MenuManager = this.RibbonForm;
            this.textEdit2.Name = "textEdit2";
            this.textEdit2.Size = new System.Drawing.Size(1331, 26);
            this.textEdit2.StyleController = this.dlcData;
            this.textEdit2.TabIndex = 52;
            // 
            // txtLugarProximaAudtextEdit1
            // 
            this.txtLugarProximaAudtextEdit1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bsEdicion, "LugarProximaAudiencia", true));
            this.txtLugarProximaAudtextEdit1.Location = new System.Drawing.Point(243, 481);
            this.txtLugarProximaAudtextEdit1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtLugarProximaAudtextEdit1.MenuManager = this.RibbonForm;
            this.txtLugarProximaAudtextEdit1.Name = "txtLugarProximaAudtextEdit1";
            this.txtLugarProximaAudtextEdit1.Size = new System.Drawing.Size(1331, 26);
            this.txtLugarProximaAudtextEdit1.StyleController = this.dlcData;
            this.txtLugarProximaAudtextEdit1.TabIndex = 50;
            // 
            // gridControl7
            // 
            this.gridControl7.DataBindings.Add(new System.Windows.Forms.Binding("DataSource", this.bsEdicion, "ExpedienteAsesorLegal", true));
            this.gridControl7.DataSource = this.bsAsesorLegall;
            this.gridControl7.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.gridControl7.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.gridControl7.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.gridControl7.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.gridControl7.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.gridControl7.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.gridControl7.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.gridControl7.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.gridControl7.EmbeddedNavigator.Buttons.NextPage.Enabled = false;
            this.gridControl7.EmbeddedNavigator.Buttons.NextPage.Visible = false;
            this.gridControl7.EmbeddedNavigator.Buttons.PrevPage.Enabled = false;
            this.gridControl7.EmbeddedNavigator.Buttons.PrevPage.Visible = false;
            this.gridControl7.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gridControl7.Location = new System.Drawing.Point(52, 353);
            this.gridControl7.MainView = this.gridView11;
            this.gridControl7.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gridControl7.MenuManager = this.RibbonForm;
            this.gridControl7.Name = "gridControl7";
            this.gridControl7.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.rpiEstudioAbogados,
            this.rpiAbogadoPorEstudio});
            this.gridControl7.Size = new System.Drawing.Size(1522, 429);
            this.gridControl7.TabIndex = 49;
            this.gridControl7.UseEmbeddedNavigator = true;
            this.gridControl7.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView11});
            // 
            // bsAsesorLegall
            // 
            this.bsAsesorLegall.DataSource = typeof(Sistema.Model.ExpedienteAsesorLegal);
            // 
            // gridView11
            // 
            this.gridView11.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colID,
            this.colIdAsesorLegalEstudio,
            this.colIdAsesorLegalAbogado,
            this.colFechaInicio1,
            this.colFechaFIn,
            this.colNroOS_RA_C});
            this.gridView11.DetailHeight = 512;
            this.gridView11.GridControl = this.gridControl7;
            this.gridView11.Name = "gridView11";
            this.gridView11.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Top;
            this.gridView11.OptionsView.ShowGroupPanel = false;
            this.gridView11.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView11_CustomRowCellEdit);
            this.gridView11.ShownEditor += new System.EventHandler(this.gridView11_ShownEditor);
            this.gridView11.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gridView11_CellValueChanged);
            // 
            // colID
            // 
            this.colID.FieldName = "ID";
            this.colID.MinWidth = 30;
            this.colID.Name = "colID";
            this.colID.Width = 112;
            // 
            // colIdAsesorLegalEstudio
            // 
            this.colIdAsesorLegalEstudio.Caption = "Patrocinante";
            this.colIdAsesorLegalEstudio.ColumnEdit = this.rpiEstudioAbogados;
            this.colIdAsesorLegalEstudio.FieldName = "IdAsesorLegalEstudio";
            this.colIdAsesorLegalEstudio.MinWidth = 30;
            this.colIdAsesorLegalEstudio.Name = "colIdAsesorLegalEstudio";
            this.colIdAsesorLegalEstudio.Visible = true;
            this.colIdAsesorLegalEstudio.VisibleIndex = 0;
            this.colIdAsesorLegalEstudio.Width = 234;
            // 
            // rpiEstudioAbogados
            // 
            this.rpiEstudioAbogados.AutoHeight = false;
            this.rpiEstudioAbogados.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rpiEstudioAbogados.Name = "rpiEstudioAbogados";
            this.rpiEstudioAbogados.PopupView = this.gridView23;
            // 
            // gridView23
            // 
            this.gridView23.DetailHeight = 512;
            this.gridView23.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView23.Name = "gridView23";
            this.gridView23.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView23.OptionsView.ShowAutoFilterRow = true;
            this.gridView23.OptionsView.ShowGroupPanel = false;
            // 
            // colIdAsesorLegalAbogado
            // 
            this.colIdAsesorLegalAbogado.Caption = "Abogado";
            this.colIdAsesorLegalAbogado.ColumnEdit = this.rpiAbogadoPorEstudio;
            this.colIdAsesorLegalAbogado.FieldName = "IdAsesorLegalAbogado";
            this.colIdAsesorLegalAbogado.MinWidth = 30;
            this.colIdAsesorLegalAbogado.Name = "colIdAsesorLegalAbogado";
            this.colIdAsesorLegalAbogado.Visible = true;
            this.colIdAsesorLegalAbogado.VisibleIndex = 1;
            this.colIdAsesorLegalAbogado.Width = 192;
            // 
            // rpiAbogadoPorEstudio
            // 
            this.rpiAbogadoPorEstudio.AutoHeight = false;
            this.rpiAbogadoPorEstudio.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rpiAbogadoPorEstudio.Name = "rpiAbogadoPorEstudio";
            this.rpiAbogadoPorEstudio.PopupView = this.repositoryItemGridLookUpEdit3View;
            this.rpiAbogadoPorEstudio.QueryPopUp += new System.ComponentModel.CancelEventHandler(this.rpiAbogadoPorEstudio_QueryPopUp);
            this.rpiAbogadoPorEstudio.Popup += new System.EventHandler(this.rpiAbogadoPorEstudio_Popup);
            // 
            // repositoryItemGridLookUpEdit3View
            // 
            this.repositoryItemGridLookUpEdit3View.DetailHeight = 512;
            this.repositoryItemGridLookUpEdit3View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.repositoryItemGridLookUpEdit3View.Name = "repositoryItemGridLookUpEdit3View";
            this.repositoryItemGridLookUpEdit3View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.repositoryItemGridLookUpEdit3View.OptionsView.ShowAutoFilterRow = true;
            this.repositoryItemGridLookUpEdit3View.OptionsView.ShowGroupPanel = false;
            // 
            // colFechaInicio1
            // 
            this.colFechaInicio1.FieldName = "FechaInicio";
            this.colFechaInicio1.MinWidth = 30;
            this.colFechaInicio1.Name = "colFechaInicio1";
            this.colFechaInicio1.Visible = true;
            this.colFechaInicio1.VisibleIndex = 2;
            this.colFechaInicio1.Width = 123;
            // 
            // colFechaFIn
            // 
            this.colFechaFIn.FieldName = "FechaFIn";
            this.colFechaFIn.MinWidth = 30;
            this.colFechaFIn.Name = "colFechaFIn";
            this.colFechaFIn.Visible = true;
            this.colFechaFIn.VisibleIndex = 3;
            this.colFechaFIn.Width = 111;
            // 
            // colNroOS_RA_C
            // 
            this.colNroOS_RA_C.Caption = "N° de OS/RA/C";
            this.colNroOS_RA_C.FieldName = "NroOS_RA_C";
            this.colNroOS_RA_C.MinWidth = 31;
            this.colNroOS_RA_C.Name = "colNroOS_RA_C";
            this.colNroOS_RA_C.Visible = true;
            this.colNroOS_RA_C.VisibleIndex = 4;
            this.colNroOS_RA_C.Width = 130;
            // 
            // btnEditarActo
            // 
            this.btnEditarActo.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("btnEditarActo.ImageOptions.SvgImage")));
            this.btnEditarActo.Location = new System.Drawing.Point(1446, 929);
            this.btnEditarActo.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnEditarActo.Name = "btnEditarActo";
            this.btnEditarActo.Size = new System.Drawing.Size(128, 56);
            this.btnEditarActo.StyleController = this.dlcData;
            this.btnEditarActo.TabIndex = 48;
            this.btnEditarActo.Text = "Editar";
            this.btnEditarActo.Click += new System.EventHandler(this.btnEditarActo_Click);
            // 
            // btnAddActo
            // 
            this.btnAddActo.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("btnAddActo.ImageOptions.SvgImage")));
            this.btnAddActo.Location = new System.Drawing.Point(1446, 867);
            this.btnAddActo.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnAddActo.Name = "btnAddActo";
            this.btnAddActo.Size = new System.Drawing.Size(128, 56);
            this.btnAddActo.StyleController = this.dlcData;
            this.btnAddActo.TabIndex = 47;
            this.btnAddActo.Text = "Agregar";
            this.btnAddActo.Click += new System.EventHandler(this.btnAddActo_Click);
            // 
            // btnEditInstancia
            // 
            this.btnEditInstancia.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("btnEditInstancia.ImageOptions.Image")));
            this.btnEditInstancia.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("btnEditInstancia.ImageOptions.SvgImage")));
            this.btnEditInstancia.Location = new System.Drawing.Point(816, 958);
            this.btnEditInstancia.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnEditInstancia.Name = "btnEditInstancia";
            this.btnEditInstancia.Size = new System.Drawing.Size(758, 56);
            this.btnEditInstancia.StyleController = this.dlcData;
            this.btnEditInstancia.TabIndex = 46;
            this.btnEditInstancia.Text = "Editar";
            this.btnEditInstancia.Click += new System.EventHandler(this.btnEditInstancia_Click);
            // 
            // simpleButton1
            // 
            this.simpleButton1.ImageOptions.SvgImage = ((DevExpress.Utils.Svg.SvgImage)(resources.GetObject("simpleButton1.ImageOptions.SvgImage")));
            this.simpleButton1.Location = new System.Drawing.Point(816, 896);
            this.simpleButton1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(758, 56);
            this.simpleButton1.StyleController = this.dlcData;
            this.simpleButton1.TabIndex = 45;
            this.simpleButton1.Text = "Agregar ";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // gridControl6
            // 
            this.gridControl6.DataSource = this.bsInstanciasExpediente;
            this.gridControl6.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gridControl6.Location = new System.Drawing.Point(52, 867);
            this.gridControl6.MainView = this.gridView27;
            this.gridControl6.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gridControl6.MenuManager = this.RibbonForm;
            this.gridControl6.Name = "gridControl6";
            this.gridControl6.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemButtonEdit2});
            this.gridControl6.Size = new System.Drawing.Size(758, 161);
            this.gridControl6.TabIndex = 44;
            this.gridControl6.Tag = "N";
            this.gridControl6.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView27});
            // 
            // bsInstanciasExpediente
            // 
            this.bsInstanciasExpediente.DataSource = typeof(Sistema.Model.ExpedienteInstancia);
            // 
            // gridView27
            // 
            this.gridView27.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colIdInstancia2,
            this.colIdOrgano2,
            this.colFechaInicio3,
            this.colFechaFinal2,
            this.colAutomisorio2,
            this.colTasacionInstancia1,
            this.colApelacion2,
            this.colObservacion3,
            this.colComentarios2,
            this.colInstanciaTexto,
            this.colOrganoTexto});
            this.gridView27.DetailHeight = 512;
            this.gridView27.GridControl = this.gridControl6;
            this.gridView27.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always;
            this.gridView27.Images = this.imgList;
            this.gridView27.Name = "gridView27";
            this.gridView27.OptionsBehavior.Editable = false;
            this.gridView27.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView27.OptionsView.ColumnAutoWidth = false;
            this.gridView27.OptionsView.ShowGroupPanel = false;
            this.gridView27.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colInstanciaTexto, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // colIdInstancia2
            // 
            this.colIdInstancia2.Caption = "Instancia";
            this.colIdInstancia2.FieldName = "InstanciaJudicial.Descripcion";
            this.colIdInstancia2.MinWidth = 30;
            this.colIdInstancia2.Name = "colIdInstancia2";
            this.colIdInstancia2.Width = 207;
            // 
            // colIdOrgano2
            // 
            this.colIdOrgano2.Caption = "Organo Judicial";
            this.colIdOrgano2.FieldName = "OrganoJudicial.Descripcion";
            this.colIdOrgano2.MinWidth = 30;
            this.colIdOrgano2.Name = "colIdOrgano2";
            this.colIdOrgano2.Width = 237;
            // 
            // colFechaInicio3
            // 
            this.colFechaInicio3.FieldName = "FechaInicio";
            this.colFechaInicio3.MinWidth = 30;
            this.colFechaInicio3.Name = "colFechaInicio3";
            this.colFechaInicio3.Visible = true;
            this.colFechaInicio3.VisibleIndex = 2;
            this.colFechaInicio3.Width = 112;
            // 
            // colFechaFinal2
            // 
            this.colFechaFinal2.FieldName = "FechaFinal";
            this.colFechaFinal2.MinWidth = 30;
            this.colFechaFinal2.Name = "colFechaFinal2";
            this.colFechaFinal2.Visible = true;
            this.colFechaFinal2.VisibleIndex = 3;
            this.colFechaFinal2.Width = 112;
            // 
            // colAutomisorio2
            // 
            this.colAutomisorio2.Caption = "Auto Admisorio";
            this.colAutomisorio2.FieldName = "Automisorio";
            this.colAutomisorio2.MinWidth = 30;
            this.colAutomisorio2.Name = "colAutomisorio2";
            this.colAutomisorio2.Visible = true;
            this.colAutomisorio2.VisibleIndex = 4;
            this.colAutomisorio2.Width = 112;
            // 
            // colTasacionInstancia1
            // 
            this.colTasacionInstancia1.FieldName = "TasacionInstancia";
            this.colTasacionInstancia1.MinWidth = 30;
            this.colTasacionInstancia1.Name = "colTasacionInstancia1";
            this.colTasacionInstancia1.Visible = true;
            this.colTasacionInstancia1.VisibleIndex = 5;
            this.colTasacionInstancia1.Width = 112;
            // 
            // colApelacion2
            // 
            this.colApelacion2.FieldName = "Apelacion";
            this.colApelacion2.MinWidth = 30;
            this.colApelacion2.Name = "colApelacion2";
            this.colApelacion2.Visible = true;
            this.colApelacion2.VisibleIndex = 6;
            this.colApelacion2.Width = 112;
            // 
            // colObservacion3
            // 
            this.colObservacion3.FieldName = "Observacion";
            this.colObservacion3.MinWidth = 30;
            this.colObservacion3.Name = "colObservacion3";
            this.colObservacion3.Visible = true;
            this.colObservacion3.VisibleIndex = 7;
            this.colObservacion3.Width = 226;
            // 
            // colComentarios2
            // 
            this.colComentarios2.FieldName = "Comentarios";
            this.colComentarios2.MinWidth = 30;
            this.colComentarios2.Name = "colComentarios2";
            this.colComentarios2.Visible = true;
            this.colComentarios2.VisibleIndex = 8;
            this.colComentarios2.Width = 279;
            // 
            // colInstanciaTexto
            // 
            this.colInstanciaTexto.Caption = "Instancia";
            this.colInstanciaTexto.FieldName = "InstanciaTexto";
            this.colInstanciaTexto.MinWidth = 30;
            this.colInstanciaTexto.Name = "colInstanciaTexto";
            this.colInstanciaTexto.Visible = true;
            this.colInstanciaTexto.VisibleIndex = 0;
            this.colInstanciaTexto.Width = 286;
            // 
            // colOrganoTexto
            // 
            this.colOrganoTexto.Caption = "Organo Judicial";
            this.colOrganoTexto.FieldName = "OrganoTexto";
            this.colOrganoTexto.MinWidth = 30;
            this.colOrganoTexto.Name = "colOrganoTexto";
            this.colOrganoTexto.Visible = true;
            this.colOrganoTexto.VisibleIndex = 1;
            this.colOrganoTexto.Width = 273;
            // 
            // repositoryItemButtonEdit2
            // 
            this.repositoryItemButtonEdit2.AutoHeight = false;
            editorButtonImageOptions15.Image = global::Sistema.UI.Properties.Resources.Search16x16;
            this.repositoryItemButtonEdit2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, editorButtonImageOptions15, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject57, serializableAppearanceObject58, serializableAppearanceObject59, serializableAppearanceObject60, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.repositoryItemButtonEdit2.Name = "repositoryItemButtonEdit2";
            // 
            // gridControl5
            // 
            this.gridControl5.DataSource = this.bsActos;
            this.gridControl5.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gridControl5.Location = new System.Drawing.Point(52, 867);
            this.gridControl5.MainView = this.gridView25;
            this.gridControl5.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gridControl5.MenuManager = this.RibbonForm;
            this.gridControl5.Name = "gridControl5";
            this.gridControl5.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemButtonEdit1});
            this.gridControl5.Size = new System.Drawing.Size(1388, 161);
            this.gridControl5.TabIndex = 43;
            this.gridControl5.Tag = "N";
            this.gridControl5.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView25});
            // 
            // gridView25
            // 
            this.gridView25.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn9,
            this.gridColumn10,
            this.gridColumn11,
            this.gridColumn12,
            this.gridColumn13,
            this.gridColumn15,
            this.gridColumn16,
            this.gridColumn17,
            this.gridColumn18,
            this.gridColumn19,
            this.gridColumn20,
            this.gridColumn21,
            this.gridColumn22,
            this.gridColumn23,
            this.colInstancia});
            this.gridView25.DetailHeight = 512;
            this.gridView25.GridControl = this.gridControl5;
            this.gridView25.HorzScrollVisibility = DevExpress.XtraGrid.Views.Base.ScrollVisibility.Always;
            this.gridView25.Images = this.imgList;
            this.gridView25.Name = "gridView25";
            this.gridView25.OptionsBehavior.Editable = false;
            this.gridView25.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView25.OptionsView.ColumnAutoWidth = false;
            this.gridView25.OptionsView.ShowGroupPanel = false;
            this.gridView25.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colInstancia, DevExpress.Data.ColumnSortOrder.Ascending),
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.gridColumn21, DevExpress.Data.ColumnSortOrder.Descending)});
            // 
            // gridColumn9
            // 
            this.gridColumn9.FieldName = "IdActoPro";
            this.gridColumn9.MinWidth = 30;
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.Width = 112;
            // 
            // gridColumn10
            // 
            this.gridColumn10.FieldName = "IdExpediente";
            this.gridColumn10.MinWidth = 30;
            this.gridColumn10.Name = "gridColumn10";
            this.gridColumn10.Width = 112;
            // 
            // gridColumn11
            // 
            this.gridColumn11.FieldName = "ActoProcesal.Contenido";
            this.gridColumn11.MinWidth = 30;
            this.gridColumn11.Name = "gridColumn11";
            this.gridColumn11.Visible = true;
            this.gridColumn11.VisibleIndex = 4;
            this.gridColumn11.Width = 502;
            // 
            // gridColumn12
            // 
            this.gridColumn12.FieldName = "Contenido1";
            this.gridColumn12.MinWidth = 30;
            this.gridColumn12.Name = "gridColumn12";
            this.gridColumn12.Width = 453;
            // 
            // gridColumn13
            // 
            this.gridColumn13.FieldName = "IdNEWID";
            this.gridColumn13.MinWidth = 30;
            this.gridColumn13.Name = "gridColumn13";
            this.gridColumn13.Width = 112;
            // 
            // gridColumn15
            // 
            this.gridColumn15.FieldName = "Expediente";
            this.gridColumn15.MinWidth = 30;
            this.gridColumn15.Name = "gridColumn15";
            this.gridColumn15.Width = 112;
            // 
            // gridColumn16
            // 
            this.gridColumn16.FieldName = "ActoProcesalContenido";
            this.gridColumn16.MinWidth = 30;
            this.gridColumn16.Name = "gridColumn16";
            this.gridColumn16.Width = 112;
            // 
            // gridColumn17
            // 
            this.gridColumn17.Caption = "Tipo Contenido ";
            this.gridColumn17.FieldName = "Expediente";
            this.gridColumn17.MinWidth = 30;
            this.gridColumn17.Name = "gridColumn17";
            this.gridColumn17.Width = 487;
            // 
            // gridColumn18
            // 
            this.gridColumn18.Caption = "Tipo Contenido";
            this.gridColumn18.FieldName = "TipoContenido.Descripcion";
            this.gridColumn18.MinWidth = 30;
            this.gridColumn18.Name = "gridColumn18";
            this.gridColumn18.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn18.Visible = true;
            this.gridColumn18.VisibleIndex = 5;
            this.gridColumn18.Width = 196;
            // 
            // gridColumn19
            // 
            this.gridColumn19.Caption = "Mostrar";
            this.gridColumn19.ColumnEdit = this.repositoryItemButtonEdit1;
            this.gridColumn19.ImageOptions.ImageIndex = 3;
            this.gridColumn19.MinWidth = 30;
            this.gridColumn19.Name = "gridColumn19";
            this.gridColumn19.OptionsColumn.AllowMerge = DevExpress.Utils.DefaultBoolean.False;
            this.gridColumn19.OptionsColumn.ShowCaption = false;
            this.gridColumn19.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.gridColumn19.Width = 51;
            // 
            // repositoryItemButtonEdit1
            // 
            this.repositoryItemButtonEdit1.AutoHeight = false;
            editorButtonImageOptions16.Image = global::Sistema.UI.Properties.Resources.Search16x16;
            this.repositoryItemButtonEdit1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, editorButtonImageOptions16, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject61, serializableAppearanceObject62, serializableAppearanceObject63, serializableAppearanceObject64, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.repositoryItemButtonEdit1.Name = "repositoryItemButtonEdit1";
            // 
            // gridColumn20
            // 
            this.gridColumn20.Caption = "Resolución";
            this.gridColumn20.FieldName = "ActoProcesal.Codigo";
            this.gridColumn20.MinWidth = 30;
            this.gridColumn20.Name = "gridColumn20";
            this.gridColumn20.Visible = true;
            this.gridColumn20.VisibleIndex = 0;
            this.gridColumn20.Width = 162;
            // 
            // gridColumn21
            // 
            this.gridColumn21.Caption = "Fecha";
            this.gridColumn21.FieldName = "ActoProcesal.FechaRegistro";
            this.gridColumn21.MinWidth = 30;
            this.gridColumn21.Name = "gridColumn21";
            this.gridColumn21.Visible = true;
            this.gridColumn21.VisibleIndex = 1;
            this.gridColumn21.Width = 100;
            // 
            // gridColumn22
            // 
            this.gridColumn22.Caption = "Recepción";
            this.gridColumn22.FieldName = "ActoProcesal.FechaRecepcion";
            this.gridColumn22.MinWidth = 30;
            this.gridColumn22.Name = "gridColumn22";
            this.gridColumn22.Visible = true;
            this.gridColumn22.VisibleIndex = 2;
            this.gridColumn22.Width = 103;
            // 
            // gridColumn23
            // 
            this.gridColumn23.Caption = "Fecha Acto";
            this.gridColumn23.FieldName = "ActoProcesal.FechaAvisoAlerta";
            this.gridColumn23.MinWidth = 30;
            this.gridColumn23.Name = "gridColumn23";
            this.gridColumn23.Visible = true;
            this.gridColumn23.VisibleIndex = 3;
            this.gridColumn23.Width = 103;
            // 
            // colInstancia
            // 
            this.colInstancia.Caption = "Instancia";
            this.colInstancia.FieldName = "ActoProcesal.DescripcionInstancia";
            this.colInstancia.MinWidth = 30;
            this.colInstancia.Name = "colInstancia";
            this.colInstancia.Width = 112;
            // 
            // glueTipoContingenciaGridLookUpEdit1
            // 
            this.glueTipoContingenciaGridLookUpEdit1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bsEdicion, "TipoContingencia", true));
            this.glueTipoContingenciaGridLookUpEdit1.Location = new System.Drawing.Point(226, 74);
            this.glueTipoContingenciaGridLookUpEdit1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.glueTipoContingenciaGridLookUpEdit1.Name = "glueTipoContingenciaGridLookUpEdit1";
            this.glueTipoContingenciaGridLookUpEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.glueTipoContingenciaGridLookUpEdit1.Properties.PopupView = this.gridView29;
            this.glueTipoContingenciaGridLookUpEdit1.Size = new System.Drawing.Size(584, 26);
            this.glueTipoContingenciaGridLookUpEdit1.StyleController = this.dlcData;
            this.glueTipoContingenciaGridLookUpEdit1.TabIndex = 42;
            this.glueTipoContingenciaGridLookUpEdit1.EditValueChanged += new System.EventHandler(this.GLueIdSupervisor_EditValueChanged);
            // 
            // gridView29
            // 
            this.gridView29.DetailHeight = 512;
            this.gridView29.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView29.Name = "gridView29";
            this.gridView29.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView29.OptionsView.ShowGroupPanel = false;
            // 
            // GLueIdSupervisor
            // 
            this.GLueIdSupervisor.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bsEdicion, "IdSupervisorINternoRela", true));
            this.GLueIdSupervisor.Location = new System.Drawing.Point(226, 170);
            this.GLueIdSupervisor.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.GLueIdSupervisor.MenuManager = this.RibbonForm;
            this.GLueIdSupervisor.Name = "GLueIdSupervisor";
            this.GLueIdSupervisor.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.GLueIdSupervisor.Properties.PopupView = this.gridView4;
            this.GLueIdSupervisor.Size = new System.Drawing.Size(584, 26);
            this.GLueIdSupervisor.StyleController = this.dlcData;
            this.GLueIdSupervisor.TabIndex = 42;
            this.GLueIdSupervisor.Tag = "N";
            this.GLueIdSupervisor.EditValueChanged += new System.EventHandler(this.GLueIdSupervisor_EditValueChanged);
            // 
            // gridView4
            // 
            this.gridView4.DetailHeight = 512;
            this.gridView4.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView4.Name = "gridView4";
            this.gridView4.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView4.OptionsView.ShowGroupPanel = false;
            // 
            // gridControl4
            // 
            this.gridControl4.DataSource = this.bsDocumentos;
            this.gridControl4.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gridControl4.Location = new System.Drawing.Point(52, 353);
            this.gridControl4.MainView = this.gridView3;
            this.gridControl4.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.gridControl4.MenuManager = this.RibbonForm;
            this.gridControl4.Name = "gridControl4";
            this.gridControl4.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.ribtnCargarDOC,
            this.ribtnShowDOC,
            this.rbtnCargarFolder,
            this.rpiglueTipoDoc,
            this.ribtnEliminarFile});
            this.gridControl4.Size = new System.Drawing.Size(1522, 429);
            this.gridControl4.TabIndex = 41;
            this.gridControl4.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView3});
            // 
            // bsDocumentos
            // 
            this.bsDocumentos.DataSource = typeof(Sistema.Model.Documento);
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn6,
            this.gridColumnCargarDoc,
            this.colGargarFolder,
            this.gridColumnMostrarDOC,
            this.colIdDocumento,
            this.colNombre,
            this.colExtension,
            this.colIdNEWID3,
            this.colDocumento1,
            this.colEliminar});
            this.gridView3.DetailHeight = 512;
            this.gridView3.GridControl = this.gridControl4;
            this.gridView3.Images = this.imgList;
            this.gridView3.Name = "gridView3";
            this.gridView3.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Top;
            this.gridView3.OptionsView.ShowGroupPanel = false;
            this.gridView3.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.colIdDocumento, DevExpress.Data.ColumnSortOrder.Descending)});
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "Tipo";
            this.gridColumn6.ColumnEdit = this.rpiglueTipoDoc;
            this.gridColumn6.FieldName = "Tipo";
            this.gridColumn6.MinWidth = 30;
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 0;
            this.gridColumn6.Width = 145;
            // 
            // rpiglueTipoDoc
            // 
            this.rpiglueTipoDoc.AutoHeight = false;
            this.rpiglueTipoDoc.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rpiglueTipoDoc.DataSource = this.bsTipoDoc;
            this.rpiglueTipoDoc.DisplayMember = "Descripcion";
            this.rpiglueTipoDoc.Name = "rpiglueTipoDoc";
            this.rpiglueTipoDoc.PopupView = this.repositoryItemGridLookUpEdit1View;
            this.rpiglueTipoDoc.ValueMember = "Descripcion";
            // 
            // bsTipoDoc
            // 
            this.bsTipoDoc.DataSource = typeof(Sistema.Model.TipoDocumentoAdd);
            // 
            // repositoryItemGridLookUpEdit1View
            // 
            this.repositoryItemGridLookUpEdit1View.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colDescripcion1});
            this.repositoryItemGridLookUpEdit1View.DetailHeight = 349;
            this.repositoryItemGridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.repositoryItemGridLookUpEdit1View.Name = "repositoryItemGridLookUpEdit1View";
            this.repositoryItemGridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.repositoryItemGridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            // 
            // colDescripcion1
            // 
            this.colDescripcion1.Caption = "Tipo";
            this.colDescripcion1.FieldName = "Descripcion";
            this.colDescripcion1.MinWidth = 19;
            this.colDescripcion1.Name = "colDescripcion1";
            this.colDescripcion1.Visible = true;
            this.colDescripcion1.VisibleIndex = 0;
            // 
            // gridColumnCargarDoc
            // 
            this.gridColumnCargarDoc.Caption = "Cargar";
            this.gridColumnCargarDoc.ColumnEdit = this.ribtnCargarDOC;
            this.gridColumnCargarDoc.ImageOptions.ImageIndex = 2;
            this.gridColumnCargarDoc.MinWidth = 30;
            this.gridColumnCargarDoc.Name = "gridColumnCargarDoc";
            this.gridColumnCargarDoc.OptionsColumn.ShowCaption = false;
            this.gridColumnCargarDoc.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.gridColumnCargarDoc.Visible = true;
            this.gridColumnCargarDoc.VisibleIndex = 1;
            this.gridColumnCargarDoc.Width = 61;
            // 
            // ribtnCargarDOC
            // 
            this.ribtnCargarDOC.AutoHeight = false;
            editorButtonImageOptions17.Image = global::Sistema.UI.Properties.Resources.Attach16x16;
            this.ribtnCargarDOC.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, editorButtonImageOptions17, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject65, serializableAppearanceObject66, serializableAppearanceObject67, serializableAppearanceObject68, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.ribtnCargarDOC.Name = "ribtnCargarDOC";
            this.ribtnCargarDOC.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.ribtnCargarDOC_ButtonClick);
            // 
            // colGargarFolder
            // 
            this.colGargarFolder.Caption = "Load";
            this.colGargarFolder.ColumnEdit = this.rbtnCargarFolder;
            this.colGargarFolder.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("colGargarFolder.ImageOptions.Image")));
            this.colGargarFolder.ImageOptions.SvgImageSize = new System.Drawing.Size(14, 0);
            this.colGargarFolder.MinWidth = 30;
            this.colGargarFolder.Name = "colGargarFolder";
            this.colGargarFolder.OptionsColumn.ShowCaption = false;
            this.colGargarFolder.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.colGargarFolder.Width = 60;
            // 
            // rbtnCargarFolder
            // 
            this.rbtnCargarFolder.AutoHeight = false;
            this.rbtnCargarFolder.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)});
            this.rbtnCargarFolder.Name = "rbtnCargarFolder";
            this.rbtnCargarFolder.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.rbtnCargarFolder_ButtonClick);
            // 
            // gridColumnMostrarDOC
            // 
            this.gridColumnMostrarDOC.Caption = "Mostrar";
            this.gridColumnMostrarDOC.ColumnEdit = this.ribtnShowDOC;
            this.gridColumnMostrarDOC.ImageOptions.ImageIndex = 3;
            this.gridColumnMostrarDOC.MinWidth = 30;
            this.gridColumnMostrarDOC.Name = "gridColumnMostrarDOC";
            this.gridColumnMostrarDOC.OptionsColumn.ShowCaption = false;
            this.gridColumnMostrarDOC.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.gridColumnMostrarDOC.Visible = true;
            this.gridColumnMostrarDOC.VisibleIndex = 4;
            this.gridColumnMostrarDOC.Width = 67;
            // 
            // ribtnShowDOC
            // 
            this.ribtnShowDOC.AutoHeight = false;
            editorButtonImageOptions18.Image = global::Sistema.UI.Properties.Resources.Search216x16;
            this.ribtnShowDOC.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph, "", -1, true, true, false, editorButtonImageOptions18, new DevExpress.Utils.KeyShortcut(System.Windows.Forms.Keys.None), serializableAppearanceObject69, serializableAppearanceObject70, serializableAppearanceObject71, serializableAppearanceObject72, "", null, null, DevExpress.Utils.ToolTipAnchor.Default)});
            this.ribtnShowDOC.Name = "ribtnShowDOC";
            this.ribtnShowDOC.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.ribtnShowDOC_ButtonClick);
            this.ribtnShowDOC.ButtonPressed += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.ribtnShowDOC_ButtonPressed);
            // 
            // colIdDocumento
            // 
            this.colIdDocumento.FieldName = "IdDocumento";
            this.colIdDocumento.MinWidth = 30;
            this.colIdDocumento.Name = "colIdDocumento";
            this.colIdDocumento.Width = 112;
            // 
            // colNombre
            // 
            this.colNombre.FieldName = "Nombre";
            this.colNombre.MinWidth = 30;
            this.colNombre.Name = "colNombre";
            this.colNombre.OptionsColumn.ReadOnly = true;
            this.colNombre.Visible = true;
            this.colNombre.VisibleIndex = 2;
            this.colNombre.Width = 387;
            // 
            // colExtension
            // 
            this.colExtension.FieldName = "Extension";
            this.colExtension.MinWidth = 30;
            this.colExtension.Name = "colExtension";
            this.colExtension.OptionsColumn.ReadOnly = true;
            this.colExtension.Visible = true;
            this.colExtension.VisibleIndex = 3;
            this.colExtension.Width = 136;
            // 
            // colIdNEWID3
            // 
            this.colIdNEWID3.FieldName = "IdNEWID";
            this.colIdNEWID3.MinWidth = 30;
            this.colIdNEWID3.Name = "colIdNEWID3";
            this.colIdNEWID3.Width = 112;
            // 
            // colDocumento1
            // 
            this.colDocumento1.FieldName = "Documento1";
            this.colDocumento1.MinWidth = 30;
            this.colDocumento1.Name = "colDocumento1";
            this.colDocumento1.OptionsColumn.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.colDocumento1.Width = 112;
            // 
            // colEliminar
            // 
            this.colEliminar.Caption = "Eliminar";
            this.colEliminar.ColumnEdit = this.ribtnEliminarFile;
            this.colEliminar.ImageOptions.Image = ((System.Drawing.Image)(resources.GetObject("colEliminar.ImageOptions.Image")));
            this.colEliminar.MinWidth = 30;
            this.colEliminar.Name = "colEliminar";
            this.colEliminar.OptionsColumn.ShowCaption = false;
            this.colEliminar.ShowButtonMode = DevExpress.XtraGrid.Views.Base.ShowButtonModeEnum.ShowAlways;
            this.colEliminar.Visible = true;
            this.colEliminar.VisibleIndex = 5;
            this.colEliminar.Width = 60;
            // 
            // ribtnEliminarFile
            // 
            this.ribtnEliminarFile.AutoHeight = false;
            this.ribtnEliminarFile.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Glyph)});
            this.ribtnEliminarFile.Name = "ribtnEliminarFile";
            this.ribtnEliminarFile.ButtonClick += new DevExpress.XtraEditors.Controls.ButtonPressedEventHandler(this.ribtnEliminarFile_ButtonClick);
            // 
            // IdExpedienteTextEdit
            // 
            this.IdExpedienteTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bsEdicion, "IdExpediente", true));
            this.IdExpedienteTextEdit.Location = new System.Drawing.Point(0, 0);
            this.IdExpedienteTextEdit.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.IdExpedienteTextEdit.MenuManager = this.RibbonForm;
            this.IdExpedienteTextEdit.Name = "IdExpedienteTextEdit";
            this.IdExpedienteTextEdit.Size = new System.Drawing.Size(0, 26);
            this.IdExpedienteTextEdit.StyleController = this.dlcData;
            this.IdExpedienteTextEdit.TabIndex = 4;
            // 
            // CodigoTextEdit
            // 
            this.CodigoTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bsEdicion, "Codigo", true));
            this.CodigoTextEdit.Location = new System.Drawing.Point(226, 74);
            this.CodigoTextEdit.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.CodigoTextEdit.MenuManager = this.RibbonForm;
            this.CodigoTextEdit.Name = "CodigoTextEdit";
            this.CodigoTextEdit.Size = new System.Drawing.Size(584, 26);
            this.CodigoTextEdit.StyleController = this.dlcData;
            this.CodigoTextEdit.TabIndex = 5;
            this.CodigoTextEdit.Tag = "N";
            // 
            // glueUbicacion
            // 
            this.glueUbicacion.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bsEdicion, "ubicacion", true));
            this.glueUbicacion.Location = new System.Drawing.Point(1007, 106);
            this.glueUbicacion.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.glueUbicacion.Name = "glueUbicacion";
            this.glueUbicacion.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.glueUbicacion.Properties.PopupView = this.gridView24;
            this.glueUbicacion.Size = new System.Drawing.Size(584, 26);
            this.glueUbicacion.StyleController = this.dlcData;
            this.glueUbicacion.TabIndex = 6;
            this.glueUbicacion.Tag = "N";
            // 
            // gridView24
            // 
            this.gridView24.DetailHeight = 512;
            this.gridView24.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView24.Name = "gridView24";
            this.gridView24.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView24.OptionsView.ShowGroupPanel = false;
            // 
            // glueModalidad
            // 
            this.glueModalidad.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bsEdicion, "Modalidad", true));
            this.glueModalidad.Location = new System.Drawing.Point(1007, 138);
            this.glueModalidad.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.glueModalidad.Name = "glueModalidad";
            this.glueModalidad.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.glueModalidad.Properties.PopupView = this.gridView22;
            this.glueModalidad.Size = new System.Drawing.Size(584, 26);
            this.glueModalidad.StyleController = this.dlcData;
            this.glueModalidad.TabIndex = 6;
            this.glueModalidad.Tag = "N";
            // 
            // gridView22
            // 
            this.gridView22.DetailHeight = 512;
            this.gridView22.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView22.Name = "gridView22";
            this.gridView22.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView22.OptionsView.ShowGroupPanel = false;
            // 
            // IdTipoProcesoGridLookUpEdit
            // 
            this.IdTipoProcesoGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bsEdicion, "DescripcionTipoArbitraje", true));
            this.IdTipoProcesoGridLookUpEdit.Location = new System.Drawing.Point(226, 170);
            this.IdTipoProcesoGridLookUpEdit.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.IdTipoProcesoGridLookUpEdit.MenuManager = this.RibbonForm;
            this.IdTipoProcesoGridLookUpEdit.Name = "IdTipoProcesoGridLookUpEdit";
            this.IdTipoProcesoGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.IdTipoProcesoGridLookUpEdit.Properties.PopupView = this.gridLookUpEdit2View;
            this.IdTipoProcesoGridLookUpEdit.Size = new System.Drawing.Size(1365, 26);
            this.IdTipoProcesoGridLookUpEdit.StyleController = this.dlcData;
            this.IdTipoProcesoGridLookUpEdit.TabIndex = 6;
            this.IdTipoProcesoGridLookUpEdit.Tag = "N";
            // 
            // gridLookUpEdit2View
            // 
            this.gridLookUpEdit2View.DetailHeight = 512;
            this.gridLookUpEdit2View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridLookUpEdit2View.Name = "gridLookUpEdit2View";
            this.gridLookUpEdit2View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit2View.OptionsView.ShowGroupPanel = false;
            // 
            // deFechaProximaAudidateEdit1
            // 
            this.deFechaProximaAudidateEdit1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bsEdicion, "FechaProximaAudiencia", true));
            this.deFechaProximaAudidateEdit1.EditValue = null;
            this.deFechaProximaAudidateEdit1.Location = new System.Drawing.Point(243, 385);
            this.deFechaProximaAudidateEdit1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.deFechaProximaAudidateEdit1.Name = "deFechaProximaAudidateEdit1";
            this.deFechaProximaAudidateEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deFechaProximaAudidateEdit1.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.deFechaProximaAudidateEdit1.Size = new System.Drawing.Size(1331, 26);
            this.deFechaProximaAudidateEdit1.StyleController = this.dlcData;
            this.deFechaProximaAudidateEdit1.TabIndex = 7;
            // 
            // deFechaVEncimientoDateEdit1
            // 
            this.deFechaVEncimientoDateEdit1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bsEdicion, "FechaVencimiento", true));
            this.deFechaVEncimientoDateEdit1.EditValue = null;
            this.deFechaVEncimientoDateEdit1.Location = new System.Drawing.Point(172, 265);
            this.deFechaVEncimientoDateEdit1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.deFechaVEncimientoDateEdit1.Name = "deFechaVEncimientoDateEdit1";
            this.deFechaVEncimientoDateEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deFechaVEncimientoDateEdit1.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.deFechaVEncimientoDateEdit1.Size = new System.Drawing.Size(638, 26);
            this.deFechaVEncimientoDateEdit1.StyleController = this.dlcData;
            this.deFechaVEncimientoDateEdit1.TabIndex = 7;
            // 
            // FechaInicioDateEdit
            // 
            this.FechaInicioDateEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bsEdicion, "FechaInicio", true));
            this.FechaInicioDateEdit.EditValue = null;
            this.FechaInicioDateEdit.Location = new System.Drawing.Point(1007, 74);
            this.FechaInicioDateEdit.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.FechaInicioDateEdit.MenuManager = this.RibbonForm;
            this.FechaInicioDateEdit.Name = "FechaInicioDateEdit";
            this.FechaInicioDateEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.FechaInicioDateEdit.Properties.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.FechaInicioDateEdit.Size = new System.Drawing.Size(584, 26);
            this.FechaInicioDateEdit.StyleController = this.dlcData;
            this.FechaInicioDateEdit.TabIndex = 7;
            this.FechaInicioDateEdit.Tag = "N";
            // 
            // IdClaseProcesoGridLookUpEdit
            // 
            this.IdClaseProcesoGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bsEdicion, "IdTipoContenidadMateria", true));
            this.IdClaseProcesoGridLookUpEdit.Location = new System.Drawing.Point(226, 138);
            this.IdClaseProcesoGridLookUpEdit.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.IdClaseProcesoGridLookUpEdit.MenuManager = this.RibbonForm;
            this.IdClaseProcesoGridLookUpEdit.Name = "IdClaseProcesoGridLookUpEdit";
            this.IdClaseProcesoGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.IdClaseProcesoGridLookUpEdit.Properties.PopupView = this.gridView5;
            this.IdClaseProcesoGridLookUpEdit.Size = new System.Drawing.Size(584, 26);
            this.IdClaseProcesoGridLookUpEdit.StyleController = this.dlcData;
            this.IdClaseProcesoGridLookUpEdit.TabIndex = 10;
            this.IdClaseProcesoGridLookUpEdit.Tag = "N";
            // 
            // gridView5
            // 
            this.gridView5.DetailHeight = 512;
            this.gridView5.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView5.Name = "gridView5";
            this.gridView5.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView5.OptionsView.ShowGroupPanel = false;
            // 
            // glueArchivadoGridLookUpEdit1
            // 
            this.glueArchivadoGridLookUpEdit1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bsEdicion, "Archivado", true));
            this.glueArchivadoGridLookUpEdit1.Location = new System.Drawing.Point(905, 265);
            this.glueArchivadoGridLookUpEdit1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.glueArchivadoGridLookUpEdit1.Name = "glueArchivadoGridLookUpEdit1";
            this.glueArchivadoGridLookUpEdit1.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.glueArchivadoGridLookUpEdit1.Properties.PopupView = this.gridView28;
            this.glueArchivadoGridLookUpEdit1.Size = new System.Drawing.Size(686, 26);
            this.glueArchivadoGridLookUpEdit1.StyleController = this.dlcData;
            this.glueArchivadoGridLookUpEdit1.TabIndex = 13;
            // 
            // gridView28
            // 
            this.gridView28.DetailHeight = 512;
            this.gridView28.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView28.Name = "gridView28";
            this.gridView28.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView28.OptionsView.ShowGroupPanel = false;
            // 
            // IdAbogadoPatrocinanteGgridLookUpEdit
            // 
            this.IdAbogadoPatrocinanteGgridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bsEdicion, "IdAbogadoPatrocinante", true));
            this.IdAbogadoPatrocinanteGgridLookUpEdit.Location = new System.Drawing.Point(226, 138);
            this.IdAbogadoPatrocinanteGgridLookUpEdit.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.IdAbogadoPatrocinanteGgridLookUpEdit.Name = "IdAbogadoPatrocinanteGgridLookUpEdit";
            this.IdAbogadoPatrocinanteGgridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.IdAbogadoPatrocinanteGgridLookUpEdit.Properties.PopupView = this.gridView26;
            this.IdAbogadoPatrocinanteGgridLookUpEdit.Size = new System.Drawing.Size(1365, 26);
            this.IdAbogadoPatrocinanteGgridLookUpEdit.StyleController = this.dlcData;
            this.IdAbogadoPatrocinanteGgridLookUpEdit.TabIndex = 13;
            // 
            // gridView26
            // 
            this.gridView26.DetailHeight = 512;
            this.gridView26.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView26.Name = "gridView26";
            this.gridView26.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView26.OptionsView.ShowGroupPanel = false;
            // 
            // IdAbogadoGridLookUpEdit
            // 
            this.IdAbogadoGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bsEdicion, "IdAbogado", true));
            this.IdAbogadoGridLookUpEdit.Location = new System.Drawing.Point(911, 74);
            this.IdAbogadoGridLookUpEdit.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.IdAbogadoGridLookUpEdit.MenuManager = this.RibbonForm;
            this.IdAbogadoGridLookUpEdit.Name = "IdAbogadoGridLookUpEdit";
            this.IdAbogadoGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.IdAbogadoGridLookUpEdit.Properties.PopupView = this.gridView6;
            this.IdAbogadoGridLookUpEdit.Size = new System.Drawing.Size(680, 26);
            this.IdAbogadoGridLookUpEdit.StyleController = this.dlcData;
            this.IdAbogadoGridLookUpEdit.TabIndex = 13;
            this.IdAbogadoGridLookUpEdit.EditValueChanged += new System.EventHandler(this.IdAbogadoGridLookUpEdit_EditValueChanged);
            // 
            // gridView6
            // 
            this.gridView6.DetailHeight = 512;
            this.gridView6.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView6.Name = "gridView6";
            this.gridView6.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView6.OptionsView.ShowGroupPanel = false;
            // 
            // IdExpedientePadreGridLookUpEdit
            // 
            this.IdExpedientePadreGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bsEdicion, "IdExpedientePadre", true));
            this.IdExpedientePadreGridLookUpEdit.Location = new System.Drawing.Point(0, 0);
            this.IdExpedientePadreGridLookUpEdit.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.IdExpedientePadreGridLookUpEdit.MenuManager = this.RibbonForm;
            this.IdExpedientePadreGridLookUpEdit.Name = "IdExpedientePadreGridLookUpEdit";
            this.IdExpedientePadreGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.IdExpedientePadreGridLookUpEdit.Properties.PopupView = this.gridView7;
            this.IdExpedientePadreGridLookUpEdit.Size = new System.Drawing.Size(0, 26);
            this.IdExpedientePadreGridLookUpEdit.StyleController = this.dlcData;
            this.IdExpedientePadreGridLookUpEdit.TabIndex = 14;
            // 
            // gridView7
            // 
            this.gridView7.DetailHeight = 512;
            this.gridView7.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView7.Name = "gridView7";
            this.gridView7.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView7.OptionsView.ShowGroupPanel = false;
            // 
            // IdNEWIDTextEdit
            // 
            this.IdNEWIDTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bsEdicion, "IdNEWID", true));
            this.IdNEWIDTextEdit.Location = new System.Drawing.Point(0, 0);
            this.IdNEWIDTextEdit.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.IdNEWIDTextEdit.MenuManager = this.RibbonForm;
            this.IdNEWIDTextEdit.Name = "IdNEWIDTextEdit";
            this.IdNEWIDTextEdit.Size = new System.Drawing.Size(0, 26);
            this.IdNEWIDTextEdit.StyleController = this.dlcData;
            this.IdNEWIDTextEdit.TabIndex = 15;
            // 
            // ActoJudicialGridLookUpEdit
            // 
            this.ActoJudicialGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bsEdicion, "ActoJudicial", true));
            this.ActoJudicialGridLookUpEdit.Location = new System.Drawing.Point(0, 0);
            this.ActoJudicialGridLookUpEdit.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ActoJudicialGridLookUpEdit.MenuManager = this.RibbonForm;
            this.ActoJudicialGridLookUpEdit.Name = "ActoJudicialGridLookUpEdit";
            this.ActoJudicialGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ActoJudicialGridLookUpEdit.Properties.PopupView = this.gridView9;
            this.ActoJudicialGridLookUpEdit.Size = new System.Drawing.Size(0, 26);
            this.ActoJudicialGridLookUpEdit.StyleController = this.dlcData;
            this.ActoJudicialGridLookUpEdit.TabIndex = 17;
            // 
            // gridView9
            // 
            this.gridView9.DetailHeight = 512;
            this.gridView9.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView9.Name = "gridView9";
            this.gridView9.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView9.OptionsView.ShowGroupPanel = false;
            // 
            // ActoProcesalGridLookUpEdit
            // 
            this.ActoProcesalGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bsEdicion, "ActoProcesal", true));
            this.ActoProcesalGridLookUpEdit.Location = new System.Drawing.Point(0, 0);
            this.ActoProcesalGridLookUpEdit.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ActoProcesalGridLookUpEdit.MenuManager = this.RibbonForm;
            this.ActoProcesalGridLookUpEdit.Name = "ActoProcesalGridLookUpEdit";
            this.ActoProcesalGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ActoProcesalGridLookUpEdit.Properties.PopupView = this.gridView10;
            this.ActoProcesalGridLookUpEdit.Size = new System.Drawing.Size(0, 26);
            this.ActoProcesalGridLookUpEdit.StyleController = this.dlcData;
            this.ActoProcesalGridLookUpEdit.TabIndex = 18;
            // 
            // gridView10
            // 
            this.gridView10.DetailHeight = 512;
            this.gridView10.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView10.Name = "gridView10";
            this.gridView10.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView10.OptionsView.ShowGroupPanel = false;
            // 
            // Expediente1GridLookUpEdit
            // 
            this.Expediente1GridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bsEdicion, "Expediente1", true));
            this.Expediente1GridLookUpEdit.Location = new System.Drawing.Point(0, 0);
            this.Expediente1GridLookUpEdit.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Expediente1GridLookUpEdit.MenuManager = this.RibbonForm;
            this.Expediente1GridLookUpEdit.Name = "Expediente1GridLookUpEdit";
            this.Expediente1GridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.Expediente1GridLookUpEdit.Properties.PopupView = this.gridView12;
            this.Expediente1GridLookUpEdit.Size = new System.Drawing.Size(0, 26);
            this.Expediente1GridLookUpEdit.StyleController = this.dlcData;
            this.Expediente1GridLookUpEdit.TabIndex = 20;
            // 
            // gridView12
            // 
            this.gridView12.DetailHeight = 512;
            this.gridView12.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView12.Name = "gridView12";
            this.gridView12.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView12.OptionsView.ShowGroupPanel = false;
            // 
            // Expediente2GridLookUpEdit
            // 
            this.Expediente2GridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bsEdicion, "Expediente2", true));
            this.Expediente2GridLookUpEdit.Location = new System.Drawing.Point(0, 0);
            this.Expediente2GridLookUpEdit.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Expediente2GridLookUpEdit.MenuManager = this.RibbonForm;
            this.Expediente2GridLookUpEdit.Name = "Expediente2GridLookUpEdit";
            this.Expediente2GridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.Expediente2GridLookUpEdit.Properties.PopupView = this.gridView13;
            this.Expediente2GridLookUpEdit.Size = new System.Drawing.Size(0, 26);
            this.Expediente2GridLookUpEdit.StyleController = this.dlcData;
            this.Expediente2GridLookUpEdit.TabIndex = 21;
            // 
            // gridView13
            // 
            this.gridView13.DetailHeight = 512;
            this.gridView13.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView13.Name = "gridView13";
            this.gridView13.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView13.OptionsView.ShowGroupPanel = false;
            // 
            // FechaMovimientoGridLookUpEdit
            // 
            this.FechaMovimientoGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bsEdicion, "FechaMovimiento", true));
            this.FechaMovimientoGridLookUpEdit.Location = new System.Drawing.Point(0, 0);
            this.FechaMovimientoGridLookUpEdit.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.FechaMovimientoGridLookUpEdit.MenuManager = this.RibbonForm;
            this.FechaMovimientoGridLookUpEdit.Name = "FechaMovimientoGridLookUpEdit";
            this.FechaMovimientoGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.FechaMovimientoGridLookUpEdit.Properties.PopupView = this.gridLookUpEdit1View;
            this.FechaMovimientoGridLookUpEdit.Size = new System.Drawing.Size(0, 26);
            this.FechaMovimientoGridLookUpEdit.StyleController = this.dlcData;
            this.FechaMovimientoGridLookUpEdit.TabIndex = 29;
            // 
            // gridLookUpEdit1View
            // 
            this.gridLookUpEdit1View.DetailHeight = 512;
            this.gridLookUpEdit1View.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridLookUpEdit1View.Name = "gridLookUpEdit1View";
            this.gridLookUpEdit1View.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridLookUpEdit1View.OptionsView.ShowGroupPanel = false;
            // 
            // NroInstanciaGridLookUpEdit
            // 
            this.NroInstanciaGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bsEdicion, "NroInstancia", true));
            this.NroInstanciaGridLookUpEdit.Location = new System.Drawing.Point(0, 0);
            this.NroInstanciaGridLookUpEdit.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.NroInstanciaGridLookUpEdit.MenuManager = this.RibbonForm;
            this.NroInstanciaGridLookUpEdit.Name = "NroInstanciaGridLookUpEdit";
            this.NroInstanciaGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.NroInstanciaGridLookUpEdit.Properties.PopupView = this.gridView8;
            this.NroInstanciaGridLookUpEdit.Size = new System.Drawing.Size(0, 26);
            this.NroInstanciaGridLookUpEdit.StyleController = this.dlcData;
            this.NroInstanciaGridLookUpEdit.TabIndex = 30;
            // 
            // gridView8
            // 
            this.gridView8.DetailHeight = 512;
            this.gridView8.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView8.Name = "gridView8";
            this.gridView8.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView8.OptionsView.ShowGroupPanel = false;
            // 
            // ClaseProcesoGridLookUpEdit
            // 
            this.ClaseProcesoGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bsEdicion, "ClaseProceso", true));
            this.ClaseProcesoGridLookUpEdit.Location = new System.Drawing.Point(0, 0);
            this.ClaseProcesoGridLookUpEdit.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ClaseProcesoGridLookUpEdit.MenuManager = this.RibbonForm;
            this.ClaseProcesoGridLookUpEdit.Name = "ClaseProcesoGridLookUpEdit";
            this.ClaseProcesoGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ClaseProcesoGridLookUpEdit.Properties.PopupView = this.gridView16;
            this.ClaseProcesoGridLookUpEdit.Size = new System.Drawing.Size(0, 26);
            this.ClaseProcesoGridLookUpEdit.StyleController = this.dlcData;
            this.ClaseProcesoGridLookUpEdit.TabIndex = 31;
            // 
            // gridView16
            // 
            this.gridView16.DetailHeight = 512;
            this.gridView16.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView16.Name = "gridView16";
            this.gridView16.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView16.OptionsView.ShowGroupPanel = false;
            // 
            // TipoProcesoGridLookUpEdit
            // 
            this.TipoProcesoGridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bsEdicion, "TipoProceso", true));
            this.TipoProcesoGridLookUpEdit.Location = new System.Drawing.Point(0, 0);
            this.TipoProcesoGridLookUpEdit.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.TipoProcesoGridLookUpEdit.MenuManager = this.RibbonForm;
            this.TipoProcesoGridLookUpEdit.Name = "TipoProcesoGridLookUpEdit";
            this.TipoProcesoGridLookUpEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.TipoProcesoGridLookUpEdit.Properties.PopupView = this.gridView17;
            this.TipoProcesoGridLookUpEdit.Size = new System.Drawing.Size(0, 26);
            this.TipoProcesoGridLookUpEdit.StyleController = this.dlcData;
            this.TipoProcesoGridLookUpEdit.TabIndex = 32;
            // 
            // gridView17
            // 
            this.gridView17.DetailHeight = 512;
            this.gridView17.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView17.Name = "gridView17";
            this.gridView17.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView17.OptionsView.ShowGroupPanel = false;
            // 
            // MontoSolesControversiaTextEdit
            // 
            this.MontoSolesControversiaTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bsEdicion, "MontoSoles", true));
            this.MontoSolesControversiaTextEdit.Location = new System.Drawing.Point(344, 216);
            this.MontoSolesControversiaTextEdit.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MontoSolesControversiaTextEdit.MenuManager = this.RibbonForm;
            this.MontoSolesControversiaTextEdit.Name = "MontoSolesControversiaTextEdit";
            this.MontoSolesControversiaTextEdit.Size = new System.Drawing.Size(188, 26);
            this.MontoSolesControversiaTextEdit.StyleController = this.dlcData;
            this.MontoSolesControversiaTextEdit.TabIndex = 35;
            // 
            // MontoDolaresTextEdit
            // 
            this.MontoDolaresTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("Text", this.bsEdicion, "MontoProbable", true));
            this.MontoDolaresTextEdit.Location = new System.Drawing.Point(1007, 106);
            this.MontoDolaresTextEdit.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MontoDolaresTextEdit.MenuManager = this.RibbonForm;
            this.MontoDolaresTextEdit.Name = "MontoDolaresTextEdit";
            this.MontoDolaresTextEdit.Size = new System.Drawing.Size(584, 26);
            this.MontoDolaresTextEdit.StyleController = this.dlcData;
            this.MontoDolaresTextEdit.TabIndex = 36;
            // 
            // OrganoExpedienteDemandanteGridControl
            // 
            this.OrganoExpedienteDemandanteGridControl.DataBindings.Add(new System.Windows.Forms.Binding("DataSource", this.bsEdicion, "OrganoExpedienteDemandante", true));
            this.OrganoExpedienteDemandanteGridControl.DataSource = this.bsDemandante;
            this.OrganoExpedienteDemandanteGridControl.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.OrganoExpedienteDemandanteGridControl.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.OrganoExpedienteDemandanteGridControl.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.OrganoExpedienteDemandanteGridControl.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.OrganoExpedienteDemandanteGridControl.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.OrganoExpedienteDemandanteGridControl.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.OrganoExpedienteDemandanteGridControl.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.OrganoExpedienteDemandanteGridControl.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.OrganoExpedienteDemandanteGridControl.EmbeddedNavigator.Buttons.NextPage.Enabled = false;
            this.OrganoExpedienteDemandanteGridControl.EmbeddedNavigator.Buttons.NextPage.Visible = false;
            this.OrganoExpedienteDemandanteGridControl.EmbeddedNavigator.Buttons.PrevPage.Enabled = false;
            this.OrganoExpedienteDemandanteGridControl.EmbeddedNavigator.Buttons.PrevPage.Visible = false;
            this.OrganoExpedienteDemandanteGridControl.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.OrganoExpedienteDemandanteGridControl.Location = new System.Drawing.Point(35, 289);
            this.OrganoExpedienteDemandanteGridControl.MainView = this.gridView18;
            this.OrganoExpedienteDemandanteGridControl.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.OrganoExpedienteDemandanteGridControl.MenuManager = this.RibbonForm;
            this.OrganoExpedienteDemandanteGridControl.Name = "OrganoExpedienteDemandanteGridControl";
            this.OrganoExpedienteDemandanteGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.rpiglueDemandante});
            this.OrganoExpedienteDemandanteGridControl.Size = new System.Drawing.Size(775, 349);
            this.OrganoExpedienteDemandanteGridControl.TabIndex = 39;
            this.OrganoExpedienteDemandanteGridControl.UseEmbeddedNavigator = true;
            this.OrganoExpedienteDemandanteGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView18});
            this.OrganoExpedienteDemandanteGridControl.Click += new System.EventHandler(this.OrganoExpedienteDemandanteGridControl_Click);
            // 
            // bsDemandante
            // 
            this.bsDemandante.DataSource = typeof(Sistema.Model.OrganoExpedientePersona);
            // 
            // gridView18
            // 
            this.gridView18.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colIdDemandante1});
            this.gridView18.DetailHeight = 512;
            this.gridView18.GridControl = this.OrganoExpedienteDemandanteGridControl;
            this.gridView18.Name = "gridView18";
            this.gridView18.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Top;
            this.gridView18.OptionsView.ShowGroupPanel = false;
            // 
            // colIdDemandante1
            // 
            this.colIdDemandante1.Caption = "Demandante(s)";
            this.colIdDemandante1.ColumnEdit = this.rpiglueDemandante;
            this.colIdDemandante1.FieldName = "IdDemandante";
            this.colIdDemandante1.MinWidth = 30;
            this.colIdDemandante1.Name = "colIdDemandante1";
            this.colIdDemandante1.Visible = true;
            this.colIdDemandante1.VisibleIndex = 0;
            this.colIdDemandante1.Width = 112;
            // 
            // rpiglueDemandante
            // 
            this.rpiglueDemandante.AutoHeight = false;
            this.rpiglueDemandante.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rpiglueDemandante.Name = "rpiglueDemandante";
            this.rpiglueDemandante.PopupView = this.gridView20;
            this.rpiglueDemandante.EditValueChanged += new System.EventHandler(this.rpiglueDemandante_EditValueChanged);
            // 
            // gridView20
            // 
            this.gridView20.DetailHeight = 512;
            this.gridView20.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView20.Name = "gridView20";
            this.gridView20.OptionsFind.AlwaysVisible = true;
            this.gridView20.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView20.OptionsView.ShowAutoFilterRow = true;
            this.gridView20.OptionsView.ShowGroupPanel = false;
            // 
            // OrganoExpedienteDemandadoGridControl
            // 
            this.OrganoExpedienteDemandadoGridControl.DataBindings.Add(new System.Windows.Forms.Binding("DataSource", this.bsEdicion, "OrganoExpedienteDemandado", true));
            this.OrganoExpedienteDemandadoGridControl.DataSource = this.bsDemandado;
            this.OrganoExpedienteDemandadoGridControl.EmbeddedNavigator.Buttons.Append.Enabled = false;
            this.OrganoExpedienteDemandadoGridControl.EmbeddedNavigator.Buttons.Append.Visible = false;
            this.OrganoExpedienteDemandadoGridControl.EmbeddedNavigator.Buttons.CancelEdit.Enabled = false;
            this.OrganoExpedienteDemandadoGridControl.EmbeddedNavigator.Buttons.CancelEdit.Visible = false;
            this.OrganoExpedienteDemandadoGridControl.EmbeddedNavigator.Buttons.Edit.Enabled = false;
            this.OrganoExpedienteDemandadoGridControl.EmbeddedNavigator.Buttons.Edit.Visible = false;
            this.OrganoExpedienteDemandadoGridControl.EmbeddedNavigator.Buttons.EndEdit.Enabled = false;
            this.OrganoExpedienteDemandadoGridControl.EmbeddedNavigator.Buttons.EndEdit.Visible = false;
            this.OrganoExpedienteDemandadoGridControl.EmbeddedNavigator.Buttons.NextPage.Enabled = false;
            this.OrganoExpedienteDemandadoGridControl.EmbeddedNavigator.Buttons.NextPage.Visible = false;
            this.OrganoExpedienteDemandadoGridControl.EmbeddedNavigator.Buttons.PrevPage.Enabled = false;
            this.OrganoExpedienteDemandadoGridControl.EmbeddedNavigator.Buttons.PrevPage.Visible = false;
            this.OrganoExpedienteDemandadoGridControl.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.OrganoExpedienteDemandadoGridControl.Location = new System.Drawing.Point(816, 289);
            this.OrganoExpedienteDemandadoGridControl.MainView = this.gridView19;
            this.OrganoExpedienteDemandadoGridControl.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.OrganoExpedienteDemandadoGridControl.MenuManager = this.RibbonForm;
            this.OrganoExpedienteDemandadoGridControl.Name = "OrganoExpedienteDemandadoGridControl";
            this.OrganoExpedienteDemandadoGridControl.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.rpiglueDemandado});
            this.OrganoExpedienteDemandadoGridControl.Size = new System.Drawing.Size(775, 349);
            this.OrganoExpedienteDemandadoGridControl.TabIndex = 40;
            this.OrganoExpedienteDemandadoGridControl.UseEmbeddedNavigator = true;
            this.OrganoExpedienteDemandadoGridControl.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView19});
            // 
            // bsDemandado
            // 
            this.bsDemandado.DataSource = typeof(Sistema.Model.OrganoExpedientePersona);
            // 
            // gridView19
            // 
            this.gridView19.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.colIdDemandado1});
            this.gridView19.DetailHeight = 512;
            this.gridView19.GridControl = this.OrganoExpedienteDemandadoGridControl;
            this.gridView19.Name = "gridView19";
            this.gridView19.OptionsView.NewItemRowPosition = DevExpress.XtraGrid.Views.Grid.NewItemRowPosition.Top;
            this.gridView19.OptionsView.ShowGroupPanel = false;
            // 
            // colIdDemandado1
            // 
            this.colIdDemandado1.Caption = "Demandado(s)";
            this.colIdDemandado1.ColumnEdit = this.rpiglueDemandado;
            this.colIdDemandado1.FieldName = "IdDemandado";
            this.colIdDemandado1.MinWidth = 30;
            this.colIdDemandado1.Name = "colIdDemandado1";
            this.colIdDemandado1.Visible = true;
            this.colIdDemandado1.VisibleIndex = 0;
            this.colIdDemandado1.Width = 112;
            // 
            // rpiglueDemandado
            // 
            this.rpiglueDemandado.AutoHeight = false;
            this.rpiglueDemandado.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.rpiglueDemandado.Name = "rpiglueDemandado";
            this.rpiglueDemandado.PopupView = this.gridView21;
            this.rpiglueDemandado.EditValueChanged += new System.EventHandler(this.rpiglueDemandado_EditValueChanged);
            // 
            // gridView21
            // 
            this.gridView21.DetailHeight = 512;
            this.gridView21.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView21.Name = "gridView21";
            this.gridView21.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView21.OptionsView.ShowAutoFilterRow = true;
            this.gridView21.OptionsView.ShowGroupPanel = false;
            // 
            // deHoraProximaAudiDateEdit2
            // 
            this.deHoraProximaAudiDateEdit2.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bsEdicion, "HoraProximaAudiencia", true));
            this.deHoraProximaAudiDateEdit2.EditValue = null;
            this.deHoraProximaAudiDateEdit2.Location = new System.Drawing.Point(243, 417);
            this.deHoraProximaAudiDateEdit2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.deHoraProximaAudiDateEdit2.Name = "deHoraProximaAudiDateEdit2";
            this.deHoraProximaAudiDateEdit2.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.deHoraProximaAudiDateEdit2.Properties.DisplayFormat.FormatString = "t";
            this.deHoraProximaAudiDateEdit2.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.deHoraProximaAudiDateEdit2.Properties.EditFormat.FormatString = "t";
            this.deHoraProximaAudiDateEdit2.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.deHoraProximaAudiDateEdit2.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.Default;
            this.deHoraProximaAudiDateEdit2.Properties.Mask.EditMask = "t";
            this.deHoraProximaAudiDateEdit2.Size = new System.Drawing.Size(1331, 26);
            this.deHoraProximaAudiDateEdit2.StyleController = this.dlcData;
            this.deHoraProximaAudiDateEdit2.TabIndex = 7;
            // 
            // glueCentroArbitral
            // 
            this.glueCentroArbitral.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bsEdicion, "NroCasillaNotificacion", true));
            this.glueCentroArbitral.Location = new System.Drawing.Point(226, 138);
            this.glueCentroArbitral.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.glueCentroArbitral.MenuManager = this.RibbonForm;
            this.glueCentroArbitral.Name = "glueCentroArbitral";
            this.glueCentroArbitral.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.glueCentroArbitral.Properties.NullText = "";
            this.glueCentroArbitral.Properties.PopupView = this.gridView32;
            this.glueCentroArbitral.Size = new System.Drawing.Size(1365, 26);
            this.glueCentroArbitral.StyleController = this.dlcData;
            this.glueCentroArbitral.TabIndex = 51;
            this.glueCentroArbitral.Tag = "N";
            // 
            // gridView32
            // 
            this.gridView32.DetailHeight = 417;
            this.gridView32.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView32.Name = "gridView32";
            this.gridView32.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView32.OptionsView.ShowGroupPanel = false;
            // 
            // EstadoActualgridLookUpEdit1
            // 
            this.EstadoActualgridLookUpEdit1.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bsEdicion, "DescripcionEstadoActual", true));
            this.EstadoActualgridLookUpEdit1.Location = new System.Drawing.Point(226, 202);
            this.EstadoActualgridLookUpEdit1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.EstadoActualgridLookUpEdit1.Name = "EstadoActualgridLookUpEdit1";
            this.EstadoActualgridLookUpEdit1.Properties.NullText = "[Vacío]";
            this.EstadoActualgridLookUpEdit1.Size = new System.Drawing.Size(584, 26);
            this.EstadoActualgridLookUpEdit1.StyleController = this.dlcData;
            this.EstadoActualgridLookUpEdit1.TabIndex = 42;
            this.EstadoActualgridLookUpEdit1.Tag = "N";
            this.EstadoActualgridLookUpEdit1.EditValueChanged += new System.EventHandler(this.GLueIdSupervisor_EditValueChanged);
            // 
            // glueContrato
            // 
            this.glueContrato.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bsEdicion, "DescripcionContrato", true));
            this.glueContrato.Location = new System.Drawing.Point(1007, 202);
            this.glueContrato.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.glueContrato.Name = "glueContrato";
            this.glueContrato.Properties.NullText = "[Vacío]";
            this.glueContrato.Size = new System.Drawing.Size(584, 26);
            this.glueContrato.StyleController = this.dlcData;
            this.glueContrato.TabIndex = 42;
            this.glueContrato.Tag = "N";
            this.glueContrato.EditValueChanged += new System.EventHandler(this.GLueIdSupervisor_EditValueChanged);
            // 
            // IdNotificacionGgridLookUpEdit
            // 
            this.IdNotificacionGgridLookUpEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bsEdicion, "CorreoParaNotificacion", true));
            this.IdNotificacionGgridLookUpEdit.Location = new System.Drawing.Point(1007, 170);
            this.IdNotificacionGgridLookUpEdit.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.IdNotificacionGgridLookUpEdit.Name = "IdNotificacionGgridLookUpEdit";
            this.IdNotificacionGgridLookUpEdit.Properties.NullText = "[Vacío]";
            this.IdNotificacionGgridLookUpEdit.Size = new System.Drawing.Size(584, 26);
            this.IdNotificacionGgridLookUpEdit.StyleController = this.dlcData;
            this.IdNotificacionGgridLookUpEdit.TabIndex = 6;
            this.IdNotificacionGgridLookUpEdit.Tag = "N";
            this.IdNotificacionGgridLookUpEdit.EditValueChanged += new System.EventHandler(this.IdNotificacionGgridLookUpEdit_EditValueChanged);
            // 
            // DescripcionTextEdit
            // 
            this.DescripcionTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bsEdicion, "Descripcion", true));
            this.DescripcionTextEdit.Location = new System.Drawing.Point(226, 234);
            this.DescripcionTextEdit.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.DescripcionTextEdit.MenuManager = this.RibbonForm;
            this.DescripcionTextEdit.Name = "DescripcionTextEdit";
            this.DescripcionTextEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.DescripcionTextEdit.Properties.NullText = "";
            this.DescripcionTextEdit.Properties.PopupView = this.gridView35;
            this.DescripcionTextEdit.Size = new System.Drawing.Size(1365, 26);
            this.DescripcionTextEdit.StyleController = this.dlcData;
            this.DescripcionTextEdit.TabIndex = 11;
            // 
            // gridView35
            // 
            this.gridView35.DetailHeight = 349;
            this.gridView35.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView35.Name = "gridView35";
            this.gridView35.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView35.OptionsView.ShowGroupPanel = false;
            // 
            // ObservacionTextEdit
            // 
            this.ObservacionTextEdit.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bsEdicion, "Observacion", true));
            this.ObservacionTextEdit.Location = new System.Drawing.Point(226, 266);
            this.ObservacionTextEdit.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ObservacionTextEdit.MenuManager = this.RibbonForm;
            this.ObservacionTextEdit.Name = "ObservacionTextEdit";
            this.ObservacionTextEdit.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ObservacionTextEdit.Properties.NullText = "";
            this.ObservacionTextEdit.Properties.PopupView = this.gridView36;
            this.ObservacionTextEdit.Size = new System.Drawing.Size(1365, 26);
            this.ObservacionTextEdit.StyleController = this.dlcData;
            this.ObservacionTextEdit.TabIndex = 12;
            // 
            // gridView36
            // 
            this.gridView36.DetailHeight = 349;
            this.gridView36.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView36.Name = "gridView36";
            this.gridView36.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView36.OptionsView.ShowGroupPanel = false;
            // 
            // txtTercerArbitroAsignado
            // 
            this.txtTercerArbitroAsignado.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bsEdicion, "TercerArbitro", true));
            this.txtTercerArbitroAsignado.Location = new System.Drawing.Point(261, 298);
            this.txtTercerArbitroAsignado.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtTercerArbitroAsignado.MenuManager = this.RibbonForm;
            this.txtTercerArbitroAsignado.Name = "txtTercerArbitroAsignado";
            this.txtTercerArbitroAsignado.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.txtTercerArbitroAsignado.Properties.NullText = "";
            this.txtTercerArbitroAsignado.Properties.PopupView = this.gridView37;
            this.txtTercerArbitroAsignado.Size = new System.Drawing.Size(1330, 26);
            this.txtTercerArbitroAsignado.StyleController = this.dlcData;
            this.txtTercerArbitroAsignado.TabIndex = 54;
            // 
            // gridView37
            // 
            this.gridView37.DetailHeight = 349;
            this.gridView37.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus;
            this.gridView37.Name = "gridView37";
            this.gridView37.OptionsSelection.EnableAppearanceFocusedCell = false;
            this.gridView37.OptionsView.ShowGroupPanel = false;
            // 
            // deFechaInstalacionTribunal
            // 
            this.deFechaInstalacionTribunal.DataBindings.Add(new System.Windows.Forms.Binding("EditValue", this.bsEdicion, "FechaInstalacionTribText", true));
            this.deFechaInstalacionTribunal.Location = new System.Drawing.Point(226, 106);
            this.deFechaInstalacionTribunal.MenuManager = this.RibbonForm;
            this.deFechaInstalacionTribunal.Name = "deFechaInstalacionTribunal";
            this.deFechaInstalacionTribunal.Size = new System.Drawing.Size(1365, 26);
            this.deFechaInstalacionTribunal.StyleController = this.dlcData;
            this.deFechaInstalacionTribunal.TabIndex = 56;
            // 
            // ItemForIdExpediente
            // 
            this.ItemForIdExpediente.Control = this.IdExpedienteTextEdit;
            this.ItemForIdExpediente.CustomizationFormText = "Id Expediente";
            this.ItemForIdExpediente.Location = new System.Drawing.Point(0, 0);
            this.ItemForIdExpediente.Name = "ItemForIdExpediente";
            this.ItemForIdExpediente.Size = new System.Drawing.Size(0, 0);
            this.ItemForIdExpediente.Text = "Id Expediente";
            this.ItemForIdExpediente.TextSize = new System.Drawing.Size(75, 31);
            // 
            // ItemForIdExpedientePadre
            // 
            this.ItemForIdExpedientePadre.Control = this.IdExpedientePadreGridLookUpEdit;
            this.ItemForIdExpedientePadre.CustomizationFormText = "Id Expediente Padre";
            this.ItemForIdExpedientePadre.Location = new System.Drawing.Point(0, 0);
            this.ItemForIdExpedientePadre.Name = "ItemForIdExpedientePadre";
            this.ItemForIdExpedientePadre.Size = new System.Drawing.Size(0, 0);
            this.ItemForIdExpedientePadre.Text = "Id Expediente Padre";
            this.ItemForIdExpedientePadre.TextSize = new System.Drawing.Size(75, 31);
            // 
            // ItemForIdNEWID
            // 
            this.ItemForIdNEWID.Control = this.IdNEWIDTextEdit;
            this.ItemForIdNEWID.CustomizationFormText = "Id NEWID";
            this.ItemForIdNEWID.Location = new System.Drawing.Point(0, 0);
            this.ItemForIdNEWID.Name = "ItemForIdNEWID";
            this.ItemForIdNEWID.Size = new System.Drawing.Size(0, 0);
            this.ItemForIdNEWID.Text = "Id NEWID";
            this.ItemForIdNEWID.TextSize = new System.Drawing.Size(75, 31);
            // 
            // ItemForActoJudicial
            // 
            this.ItemForActoJudicial.Control = this.ActoJudicialGridLookUpEdit;
            this.ItemForActoJudicial.CustomizationFormText = "Acto Judicial";
            this.ItemForActoJudicial.Location = new System.Drawing.Point(0, 0);
            this.ItemForActoJudicial.Name = "ItemForActoJudicial";
            this.ItemForActoJudicial.Size = new System.Drawing.Size(0, 0);
            this.ItemForActoJudicial.Text = "Acto Judicial";
            this.ItemForActoJudicial.TextSize = new System.Drawing.Size(75, 31);
            // 
            // ItemForActoProcesal
            // 
            this.ItemForActoProcesal.Control = this.ActoProcesalGridLookUpEdit;
            this.ItemForActoProcesal.CustomizationFormText = "Acto Procesal";
            this.ItemForActoProcesal.Location = new System.Drawing.Point(0, 0);
            this.ItemForActoProcesal.Name = "ItemForActoProcesal";
            this.ItemForActoProcesal.Size = new System.Drawing.Size(0, 0);
            this.ItemForActoProcesal.Text = "Acto Procesal";
            this.ItemForActoProcesal.TextSize = new System.Drawing.Size(75, 31);
            // 
            // ItemForExpediente1
            // 
            this.ItemForExpediente1.Control = this.Expediente1GridLookUpEdit;
            this.ItemForExpediente1.CustomizationFormText = "Expediente1";
            this.ItemForExpediente1.Location = new System.Drawing.Point(0, 0);
            this.ItemForExpediente1.Name = "ItemForExpediente1";
            this.ItemForExpediente1.Size = new System.Drawing.Size(0, 0);
            this.ItemForExpediente1.Text = "Expediente1";
            this.ItemForExpediente1.TextSize = new System.Drawing.Size(75, 31);
            // 
            // ItemForExpediente2
            // 
            this.ItemForExpediente2.Control = this.Expediente2GridLookUpEdit;
            this.ItemForExpediente2.CustomizationFormText = "Expediente2";
            this.ItemForExpediente2.Location = new System.Drawing.Point(0, 0);
            this.ItemForExpediente2.Name = "ItemForExpediente2";
            this.ItemForExpediente2.Size = new System.Drawing.Size(0, 0);
            this.ItemForExpediente2.Text = "Expediente2";
            this.ItemForExpediente2.TextSize = new System.Drawing.Size(75, 31);
            // 
            // ItemForFechaMovimiento
            // 
            this.ItemForFechaMovimiento.Control = this.FechaMovimientoGridLookUpEdit;
            this.ItemForFechaMovimiento.CustomizationFormText = "Fecha Movimiento";
            this.ItemForFechaMovimiento.Location = new System.Drawing.Point(0, 0);
            this.ItemForFechaMovimiento.Name = "ItemForFechaMovimiento";
            this.ItemForFechaMovimiento.Size = new System.Drawing.Size(0, 0);
            this.ItemForFechaMovimiento.Text = "Fecha Movimiento";
            this.ItemForFechaMovimiento.TextSize = new System.Drawing.Size(75, 31);
            // 
            // ItemForNroInstancia
            // 
            this.ItemForNroInstancia.Control = this.NroInstanciaGridLookUpEdit;
            this.ItemForNroInstancia.CustomizationFormText = "Nro Instancia";
            this.ItemForNroInstancia.Location = new System.Drawing.Point(0, 0);
            this.ItemForNroInstancia.Name = "ItemForNroInstancia";
            this.ItemForNroInstancia.Size = new System.Drawing.Size(0, 0);
            this.ItemForNroInstancia.Text = "Nro Instancia";
            this.ItemForNroInstancia.TextSize = new System.Drawing.Size(75, 31);
            // 
            // ItemForClaseProceso
            // 
            this.ItemForClaseProceso.Control = this.ClaseProcesoGridLookUpEdit;
            this.ItemForClaseProceso.CustomizationFormText = "Clase Proceso";
            this.ItemForClaseProceso.Location = new System.Drawing.Point(0, 0);
            this.ItemForClaseProceso.Name = "ItemForClaseProceso";
            this.ItemForClaseProceso.Size = new System.Drawing.Size(0, 0);
            this.ItemForClaseProceso.Text = "Clase Proceso";
            this.ItemForClaseProceso.TextSize = new System.Drawing.Size(75, 31);
            // 
            // ItemForTipoProceso
            // 
            this.ItemForTipoProceso.Control = this.TipoProcesoGridLookUpEdit;
            this.ItemForTipoProceso.CustomizationFormText = "Tipo Proceso";
            this.ItemForTipoProceso.Location = new System.Drawing.Point(0, 0);
            this.ItemForTipoProceso.Name = "ItemForTipoProceso";
            this.ItemForTipoProceso.Size = new System.Drawing.Size(0, 0);
            this.ItemForTipoProceso.Text = "Tipo Proceso";
            this.ItemForTipoProceso.TextSize = new System.Drawing.Size(75, 31);
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "layoutControlGroup1";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup3,
            this.splitterItem3});
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(1626, 1101);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.AllowDrawBackground = false;
            this.layoutControlGroup3.CustomizationFormText = "autoGeneratedGroup0";
            this.layoutControlGroup3.GroupBordersVisible = false;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lcgTabsEdicion});
            this.layoutControlGroup3.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup3.Name = "autoGeneratedGroup0";
            this.layoutControlGroup3.Size = new System.Drawing.Size(1596, 1062);
            // 
            // lcgTabsEdicion
            // 
            this.lcgTabsEdicion.Location = new System.Drawing.Point(0, 0);
            this.lcgTabsEdicion.Name = "lcgTabsEdicion";
            this.lcgTabsEdicion.SelectedTabPage = this.lcgEstadoArbitral;
            this.lcgTabsEdicion.Size = new System.Drawing.Size(1596, 1062);
            this.lcgTabsEdicion.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.lcgDatosGenerales,
            this.lcgDatosArbitrajes,
            this.lcgEstadoArbitral});
            // 
            // lcgEstadoArbitral
            // 
            this.lcgEstadoArbitral.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem20,
            this.ItemForIdAbogado,
            this.layoutControlItem41,
            this.ItemForMontoDolares,
            this.layoutControlItem12,
            this.layoutControlItem24,
            this.layoutControlItem19,
            this.tabbedControlGroup2,
            this.layoutControlGroup10,
            this.layoutControlGroup11,
            this.layoutControlGroup12,
            this.tabbedControlGroup1});
            this.lcgEstadoArbitral.Location = new System.Drawing.Point(0, 0);
            this.lcgEstadoArbitral.Name = "lcgEstadoArbitral";
            this.lcgEstadoArbitral.Size = new System.Drawing.Size(1562, 983);
            this.lcgEstadoArbitral.Text = "ESTADO ARBITRAL";
            // 
            // layoutControlItem20
            // 
            this.layoutControlItem20.Control = this.glueTipoContingenciaGridLookUpEdit1;
            this.layoutControlItem20.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem20.Name = "layoutControlItem20";
            this.layoutControlItem20.Size = new System.Drawing.Size(781, 32);
            this.layoutControlItem20.Text = "Contingencia";
            this.layoutControlItem20.TextSize = new System.Drawing.Size(185, 19);
            // 
            // ItemForIdAbogado
            // 
            this.ItemForIdAbogado.Control = this.IdAbogadoGridLookUpEdit;
            this.ItemForIdAbogado.CustomizationFormText = "Abogado";
            this.ItemForIdAbogado.Location = new System.Drawing.Point(781, 0);
            this.ItemForIdAbogado.Name = "ItemForIdAbogado";
            this.ItemForIdAbogado.Size = new System.Drawing.Size(781, 32);
            this.ItemForIdAbogado.Text = "Asesor Legal";
            this.ItemForIdAbogado.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.ItemForIdAbogado.TextSize = new System.Drawing.Size(90, 19);
            this.ItemForIdAbogado.TextToControlDistance = 5;
            this.ItemForIdAbogado.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // layoutControlItem41
            // 
            this.layoutControlItem41.Control = this.txtJustificacionContingencia;
            this.layoutControlItem41.Location = new System.Drawing.Point(0, 32);
            this.layoutControlItem41.Name = "layoutControlItem41";
            this.layoutControlItem41.Size = new System.Drawing.Size(781, 32);
            this.layoutControlItem41.Text = "Justificación Contingencia";
            this.layoutControlItem41.TextSize = new System.Drawing.Size(185, 19);
            // 
            // ItemForMontoDolares
            // 
            this.ItemForMontoDolares.Control = this.MontoDolaresTextEdit;
            this.ItemForMontoDolares.CustomizationFormText = "Monto Dolares";
            this.ItemForMontoDolares.Location = new System.Drawing.Point(781, 32);
            this.ItemForMontoDolares.Name = "ItemForMontoDolares";
            this.ItemForMontoDolares.Size = new System.Drawing.Size(781, 32);
            this.ItemForMontoDolares.Text = "Monto Probable";
            this.ItemForMontoDolares.TextSize = new System.Drawing.Size(185, 19);
            this.ItemForMontoDolares.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.IdAbogadoPatrocinanteGgridLookUpEdit;
            this.layoutControlItem12.Location = new System.Drawing.Point(0, 64);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(1562, 32);
            this.layoutControlItem12.Text = "Abogado Patrocinante";
            this.layoutControlItem12.TextSize = new System.Drawing.Size(185, 19);
            this.layoutControlItem12.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // layoutControlItem24
            // 
            this.layoutControlItem24.Control = this.deFechaVEncimientoDateEdit1;
            this.layoutControlItem24.Location = new System.Drawing.Point(0, 191);
            this.layoutControlItem24.Name = "layoutControlItem24";
            this.layoutControlItem24.Size = new System.Drawing.Size(781, 32);
            this.layoutControlItem24.Text = "Fecha Vencimiento";
            this.layoutControlItem24.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem24.TextSize = new System.Drawing.Size(132, 19);
            this.layoutControlItem24.TextToControlDistance = 5;
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.Control = this.glueArchivadoGridLookUpEdit1;
            this.layoutControlItem19.Location = new System.Drawing.Point(781, 191);
            this.layoutControlItem19.Name = "layoutControlItem19";
            this.layoutControlItem19.Size = new System.Drawing.Size(781, 32);
            this.layoutControlItem19.Text = "Archivado ?";
            this.layoutControlItem19.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem19.TextSize = new System.Drawing.Size(84, 19);
            this.layoutControlItem19.TextToControlDistance = 5;
            // 
            // tabbedControlGroup2
            // 
            this.tabbedControlGroup2.Location = new System.Drawing.Point(0, 223);
            this.tabbedControlGroup2.Name = "tabbedControlGroup2";
            this.tabbedControlGroup2.SelectedTabPage = this.layoutControlGroup8;
            this.tabbedControlGroup2.Size = new System.Drawing.Size(1562, 514);
            this.tabbedControlGroup2.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup8,
            this.layoutControlGroup9,
            this.layoutControlGroup6});
            // 
            // layoutControlGroup8
            // 
            this.layoutControlGroup8.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem18});
            this.layoutControlGroup8.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup8.Name = "layoutControlGroup8";
            this.layoutControlGroup8.Size = new System.Drawing.Size(1528, 435);
            this.layoutControlGroup8.Text = "PATROCINANTE";
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.Control = this.gridControl7;
            this.layoutControlItem18.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem18.Name = "layoutControlItem18";
            this.layoutControlItem18.Size = new System.Drawing.Size(1528, 435);
            this.layoutControlItem18.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem18.TextVisible = false;
            // 
            // layoutControlGroup9
            // 
            this.layoutControlGroup9.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem21,
            this.layoutControlItem22,
            this.layoutControlItem23,
            this.layoutControlItem27,
            this.layoutControlItem26,
            this.layoutControlItem28,
            this.emptySpaceItem7});
            this.layoutControlGroup9.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup9.Name = "layoutControlGroup9";
            this.layoutControlGroup9.Size = new System.Drawing.Size(1528, 435);
            this.layoutControlGroup9.Text = "NOTIFICACIONES";
            this.layoutControlGroup9.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // layoutControlItem21
            // 
            this.layoutControlItem21.Control = this.deFechaProximaAudidateEdit1;
            this.layoutControlItem21.Location = new System.Drawing.Point(0, 32);
            this.layoutControlItem21.Name = "layoutControlItem21";
            this.layoutControlItem21.Size = new System.Drawing.Size(1528, 32);
            this.layoutControlItem21.Text = "Fecha";
            this.layoutControlItem21.TextSize = new System.Drawing.Size(185, 19);
            // 
            // layoutControlItem22
            // 
            this.layoutControlItem22.Control = this.deHoraProximaAudiDateEdit2;
            this.layoutControlItem22.Location = new System.Drawing.Point(0, 64);
            this.layoutControlItem22.Name = "layoutControlItem22";
            this.layoutControlItem22.Size = new System.Drawing.Size(1528, 32);
            this.layoutControlItem22.Text = "Hora";
            this.layoutControlItem22.TextSize = new System.Drawing.Size(185, 19);
            // 
            // layoutControlItem23
            // 
            this.layoutControlItem23.Control = this.txtLugarProximaAudtextEdit1;
            this.layoutControlItem23.Location = new System.Drawing.Point(0, 128);
            this.layoutControlItem23.Name = "layoutControlItem23";
            this.layoutControlItem23.Size = new System.Drawing.Size(1528, 32);
            this.layoutControlItem23.Text = "Lugar";
            this.layoutControlItem23.TextSize = new System.Drawing.Size(185, 19);
            this.layoutControlItem23.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // layoutControlItem27
            // 
            this.layoutControlItem27.Control = this.textEdit3;
            this.layoutControlItem27.Location = new System.Drawing.Point(0, 160);
            this.layoutControlItem27.Name = "layoutControlItem27";
            this.layoutControlItem27.Size = new System.Drawing.Size(1528, 32);
            this.layoutControlItem27.Text = "Nro. Dias";
            this.layoutControlItem27.TextSize = new System.Drawing.Size(185, 19);
            // 
            // layoutControlItem26
            // 
            this.layoutControlItem26.Control = this.textEdit2;
            this.layoutControlItem26.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem26.Name = "layoutControlItem26";
            this.layoutControlItem26.Size = new System.Drawing.Size(1528, 32);
            this.layoutControlItem26.Text = "Tipo";
            this.layoutControlItem26.TextSize = new System.Drawing.Size(185, 19);
            // 
            // layoutControlItem28
            // 
            this.layoutControlItem28.Control = this.LugarGridLookUpEdit;
            this.layoutControlItem28.Location = new System.Drawing.Point(0, 96);
            this.layoutControlItem28.Name = "layoutControlItem28";
            this.layoutControlItem28.Size = new System.Drawing.Size(1528, 32);
            this.layoutControlItem28.Text = "Lugar";
            this.layoutControlItem28.TextSize = new System.Drawing.Size(185, 19);
            // 
            // emptySpaceItem7
            // 
            this.emptySpaceItem7.AllowHotTrack = false;
            this.emptySpaceItem7.Location = new System.Drawing.Point(0, 192);
            this.emptySpaceItem7.Name = "emptySpaceItem7";
            this.emptySpaceItem7.Size = new System.Drawing.Size(1528, 243);
            this.emptySpaceItem7.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.CustomizationFormText = "Archivos";
            this.layoutControlGroup6.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem9});
            this.layoutControlGroup6.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup6.Name = "layoutControlGroup6";
            this.layoutControlGroup6.Size = new System.Drawing.Size(1528, 435);
            this.layoutControlGroup6.Text = "PIEZAS PROCESALES";
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.gridControl4;
            this.layoutControlItem9.CustomizationFormText = "Archivos";
            this.layoutControlItem9.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(1528, 435);
            this.layoutControlItem9.Text = "Archivos";
            this.layoutControlItem9.TextLocation = DevExpress.Utils.Locations.Top;
            this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem9.TextVisible = false;
            // 
            // layoutControlGroup10
            // 
            this.layoutControlGroup10.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem34,
            this.ItemForMontoSoles});
            this.layoutControlGroup10.Location = new System.Drawing.Point(0, 96);
            this.layoutControlGroup10.Name = "layoutControlGroup10";
            this.layoutControlGroup10.Size = new System.Drawing.Size(520, 95);
            this.layoutControlGroup10.Text = "Cuantía Controversia";
            // 
            // layoutControlItem34
            // 
            this.layoutControlItem34.Control = this.glueTipoMonedaCOntrovercia;
            this.layoutControlItem34.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem34.Name = "layoutControlItem34";
            this.layoutControlItem34.Size = new System.Drawing.Size(243, 32);
            this.layoutControlItem34.Text = "Tipo Moneda";
            this.layoutControlItem34.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem34.TextSize = new System.Drawing.Size(92, 19);
            this.layoutControlItem34.TextToControlDistance = 5;
            // 
            // ItemForMontoSoles
            // 
            this.ItemForMontoSoles.Control = this.MontoSolesControversiaTextEdit;
            this.ItemForMontoSoles.CustomizationFormText = "Monto Soles";
            this.ItemForMontoSoles.Location = new System.Drawing.Point(243, 0);
            this.ItemForMontoSoles.Name = "ItemForMontoSoles";
            this.ItemForMontoSoles.Size = new System.Drawing.Size(243, 32);
            this.ItemForMontoSoles.Text = "Monto";
            this.ItemForMontoSoles.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.ItemForMontoSoles.TextSize = new System.Drawing.Size(44, 19);
            this.ItemForMontoSoles.TextToControlDistance = 5;
            // 
            // layoutControlGroup11
            // 
            this.layoutControlGroup11.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem35,
            this.layoutControlItem38});
            this.layoutControlGroup11.Location = new System.Drawing.Point(520, 96);
            this.layoutControlGroup11.Name = "layoutControlGroup11";
            this.layoutControlGroup11.Size = new System.Drawing.Size(519, 95);
            this.layoutControlGroup11.Text = "Reconvención  ";
            // 
            // layoutControlItem35
            // 
            this.layoutControlItem35.Control = this.glueTipoMonedaReconvencion;
            this.layoutControlItem35.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem35.Name = "layoutControlItem35";
            this.layoutControlItem35.Size = new System.Drawing.Size(242, 32);
            this.layoutControlItem35.Text = "Tipo Moneda";
            this.layoutControlItem35.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem35.TextSize = new System.Drawing.Size(92, 19);
            this.layoutControlItem35.TextToControlDistance = 5;
            // 
            // layoutControlItem38
            // 
            this.layoutControlItem38.Control = this.txtMontoReconvencion;
            this.layoutControlItem38.Location = new System.Drawing.Point(242, 0);
            this.layoutControlItem38.Name = "layoutControlItem38";
            this.layoutControlItem38.Size = new System.Drawing.Size(243, 32);
            this.layoutControlItem38.Text = "Monto";
            this.layoutControlItem38.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem38.TextSize = new System.Drawing.Size(44, 19);
            this.layoutControlItem38.TextToControlDistance = 5;
            // 
            // layoutControlGroup12
            // 
            this.layoutControlGroup12.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem37,
            this.layoutControlItem36});
            this.layoutControlGroup12.Location = new System.Drawing.Point(1039, 96);
            this.layoutControlGroup12.Name = "layoutControlGroup12";
            this.layoutControlGroup12.Size = new System.Drawing.Size(523, 95);
            this.layoutControlGroup12.Text = "Monto Laudado";
            // 
            // layoutControlItem37
            // 
            this.layoutControlItem37.Control = this.glueTipoMonedaLaudado;
            this.layoutControlItem37.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem37.Name = "layoutControlItem37";
            this.layoutControlItem37.Size = new System.Drawing.Size(244, 32);
            this.layoutControlItem37.Text = "Tipo Moneda";
            this.layoutControlItem37.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem37.TextSize = new System.Drawing.Size(92, 19);
            this.layoutControlItem37.TextToControlDistance = 5;
            // 
            // layoutControlItem36
            // 
            this.layoutControlItem36.Control = this.txtMontoLaudado;
            this.layoutControlItem36.Location = new System.Drawing.Point(244, 0);
            this.layoutControlItem36.Name = "layoutControlItem36";
            this.layoutControlItem36.Size = new System.Drawing.Size(245, 32);
            this.layoutControlItem36.Text = "Monto";
            this.layoutControlItem36.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem36.TextSize = new System.Drawing.Size(44, 19);
            this.layoutControlItem36.TextToControlDistance = 5;
            // 
            // tabbedControlGroup1
            // 
            this.tabbedControlGroup1.Location = new System.Drawing.Point(0, 737);
            this.tabbedControlGroup1.Name = "tabbedControlGroup1";
            this.tabbedControlGroup1.SelectedTabPage = this.layoutControlGroup4;
            this.tabbedControlGroup1.Size = new System.Drawing.Size(1562, 246);
            this.tabbedControlGroup1.TabPages.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlGroup7,
            this.layoutControlGroup4});
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem11,
            this.layoutControlItem16,
            this.layoutControlItem17,
            this.emptySpaceItem5});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup4.Name = "layoutControlGroup4";
            this.layoutControlGroup4.Size = new System.Drawing.Size(1528, 167);
            this.layoutControlGroup4.Text = "ACTOS PROCESALES";
            // 
            // layoutControlItem11
            // 
            this.layoutControlItem11.Control = this.gridControl5;
            this.layoutControlItem11.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem11.Name = "layoutControlItem11";
            this.layoutControlItem11.Size = new System.Drawing.Size(1394, 167);
            this.layoutControlItem11.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem11.TextVisible = false;
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.Control = this.btnAddActo;
            this.layoutControlItem16.Location = new System.Drawing.Point(1394, 0);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Size = new System.Drawing.Size(134, 62);
            this.layoutControlItem16.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem16.TextVisible = false;
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.Control = this.btnEditarActo;
            this.layoutControlItem17.Location = new System.Drawing.Point(1394, 62);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Size = new System.Drawing.Size(134, 62);
            this.layoutControlItem17.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem17.TextVisible = false;
            // 
            // emptySpaceItem5
            // 
            this.emptySpaceItem5.AllowHotTrack = false;
            this.emptySpaceItem5.Location = new System.Drawing.Point(1394, 124);
            this.emptySpaceItem5.Name = "emptySpaceItem5";
            this.emptySpaceItem5.Size = new System.Drawing.Size(134, 43);
            this.emptySpaceItem5.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlGroup7
            // 
            this.layoutControlGroup7.CustomizationFormText = "INSTANCIAS";
            this.layoutControlGroup7.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem13,
            this.emptySpaceItem3,
            this.layoutControlItem14,
            this.layoutControlItem15,
            this.emptySpaceItem4});
            this.layoutControlGroup7.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup7.Name = "layoutControlGroup7";
            this.layoutControlGroup7.Size = new System.Drawing.Size(1528, 167);
            this.layoutControlGroup7.Text = "INSTANCIAS";
            this.layoutControlGroup7.Visibility = DevExpress.XtraLayout.Utils.LayoutVisibility.Never;
            // 
            // layoutControlItem13
            // 
            this.layoutControlItem13.Control = this.gridControl6;
            this.layoutControlItem13.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem13.Name = "layoutControlItem13";
            this.layoutControlItem13.Size = new System.Drawing.Size(764, 167);
            this.layoutControlItem13.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem13.TextVisible = false;
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.Location = new System.Drawing.Point(764, 153);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(764, 14);
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem14
            // 
            this.layoutControlItem14.Control = this.simpleButton1;
            this.layoutControlItem14.Location = new System.Drawing.Point(764, 29);
            this.layoutControlItem14.Name = "layoutControlItem14";
            this.layoutControlItem14.Size = new System.Drawing.Size(764, 62);
            this.layoutControlItem14.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem14.TextVisible = false;
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.Control = this.btnEditInstancia;
            this.layoutControlItem15.Location = new System.Drawing.Point(764, 91);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(764, 62);
            this.layoutControlItem15.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem15.TextVisible = false;
            // 
            // emptySpaceItem4
            // 
            this.emptySpaceItem4.AllowHotTrack = false;
            this.emptySpaceItem4.Location = new System.Drawing.Point(764, 0);
            this.emptySpaceItem4.Name = "emptySpaceItem4";
            this.emptySpaceItem4.Size = new System.Drawing.Size(764, 29);
            this.emptySpaceItem4.TextSize = new System.Drawing.Size(0, 0);
            // 
            // lcgDatosGenerales
            // 
            this.lcgDatosGenerales.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForCodigo,
            this.ItemForFechaInicio,
            this.layoutControlItem30,
            this.ItemForIdClaseProceso,
            this.layoutControlItem32,
            this.layoutControlItem31,
            this.layoutControlItem5,
            this.layoutControlItem10,
            this.layoutControlItem25,
            this.ItemForOrganoExpedienteDemandante,
            this.ItemForOrganoExpedienteDemandado,
            this.layoutControlItem39,
            this.layoutControlItem6,
            this.emptySpaceItem6});
            this.lcgDatosGenerales.Location = new System.Drawing.Point(0, 0);
            this.lcgDatosGenerales.Name = "lcgDatosGenerales";
            this.lcgDatosGenerales.Size = new System.Drawing.Size(1562, 983);
            this.lcgDatosGenerales.Text = "DATOS GENERALES";
            // 
            // ItemForCodigo
            // 
            this.ItemForCodigo.Control = this.CodigoTextEdit;
            this.ItemForCodigo.CustomizationFormText = "Codigo";
            this.ItemForCodigo.Location = new System.Drawing.Point(0, 0);
            this.ItemForCodigo.Name = "ItemForCodigo";
            this.ItemForCodigo.Size = new System.Drawing.Size(781, 32);
            this.ItemForCodigo.Text = "Nro. de Expediente";
            this.ItemForCodigo.TextSize = new System.Drawing.Size(185, 19);
            // 
            // ItemForFechaInicio
            // 
            this.ItemForFechaInicio.Control = this.FechaInicioDateEdit;
            this.ItemForFechaInicio.CustomizationFormText = "Fecha Inicio";
            this.ItemForFechaInicio.Location = new System.Drawing.Point(781, 0);
            this.ItemForFechaInicio.Name = "ItemForFechaInicio";
            this.ItemForFechaInicio.Size = new System.Drawing.Size(781, 32);
            this.ItemForFechaInicio.Text = "Fecha Inicio";
            this.ItemForFechaInicio.TextSize = new System.Drawing.Size(185, 19);
            // 
            // layoutControlItem30
            // 
            this.layoutControlItem30.Control = this.txtTipoDocumento;
            this.layoutControlItem30.Location = new System.Drawing.Point(0, 32);
            this.layoutControlItem30.Name = "layoutControlItem30";
            this.layoutControlItem30.Size = new System.Drawing.Size(781, 32);
            this.layoutControlItem30.Text = "Tipo Documento";
            this.layoutControlItem30.TextSize = new System.Drawing.Size(185, 19);
            // 
            // ItemForIdClaseProceso
            // 
            this.ItemForIdClaseProceso.Control = this.IdClaseProcesoGridLookUpEdit;
            this.ItemForIdClaseProceso.CustomizationFormText = "Clase Proceso";
            this.ItemForIdClaseProceso.Location = new System.Drawing.Point(0, 64);
            this.ItemForIdClaseProceso.Name = "ItemForIdClaseProceso";
            this.ItemForIdClaseProceso.Size = new System.Drawing.Size(781, 32);
            this.ItemForIdClaseProceso.Text = "Materia";
            this.ItemForIdClaseProceso.TextSize = new System.Drawing.Size(185, 19);
            // 
            // layoutControlItem32
            // 
            this.layoutControlItem32.Control = this.glueUbicacion;
            this.layoutControlItem32.Location = new System.Drawing.Point(781, 32);
            this.layoutControlItem32.Name = "layoutControlItem32";
            this.layoutControlItem32.Size = new System.Drawing.Size(781, 32);
            this.layoutControlItem32.Text = "Ubicación";
            this.layoutControlItem32.TextSize = new System.Drawing.Size(185, 19);
            // 
            // layoutControlItem31
            // 
            this.layoutControlItem31.Control = this.glueModalidad;
            this.layoutControlItem31.Location = new System.Drawing.Point(781, 64);
            this.layoutControlItem31.Name = "layoutControlItem31";
            this.layoutControlItem31.Size = new System.Drawing.Size(781, 32);
            this.layoutControlItem31.Text = "Modalidad";
            this.layoutControlItem31.TextSize = new System.Drawing.Size(185, 19);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.GLueIdSupervisor;
            this.layoutControlItem5.Location = new System.Drawing.Point(0, 96);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(781, 32);
            this.layoutControlItem5.Text = "Supervisor Interno";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(185, 19);
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.IdNotificacionGgridLookUpEdit;
            this.layoutControlItem10.Location = new System.Drawing.Point(781, 96);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(781, 32);
            this.layoutControlItem10.Text = "Correos para Notificación ";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(185, 19);
            // 
            // layoutControlItem25
            // 
            this.layoutControlItem25.Control = this.EstadoActualgridLookUpEdit1;
            this.layoutControlItem25.Location = new System.Drawing.Point(0, 128);
            this.layoutControlItem25.Name = "layoutControlItem25";
            this.layoutControlItem25.Size = new System.Drawing.Size(781, 32);
            this.layoutControlItem25.Text = "Estado del Proceso";
            this.layoutControlItem25.TextSize = new System.Drawing.Size(185, 19);
            // 
            // ItemForOrganoExpedienteDemandante
            // 
            this.ItemForOrganoExpedienteDemandante.Control = this.OrganoExpedienteDemandanteGridControl;
            this.ItemForOrganoExpedienteDemandante.CustomizationFormText = "Demandante";
            this.ItemForOrganoExpedienteDemandante.Location = new System.Drawing.Point(0, 192);
            this.ItemForOrganoExpedienteDemandante.Name = "ItemForOrganoExpedienteDemandante";
            this.ItemForOrganoExpedienteDemandante.Size = new System.Drawing.Size(781, 378);
            this.ItemForOrganoExpedienteDemandante.Text = "Demandante (s)";
            this.ItemForOrganoExpedienteDemandante.TextLocation = DevExpress.Utils.Locations.Top;
            this.ItemForOrganoExpedienteDemandante.TextSize = new System.Drawing.Size(185, 19);
            // 
            // ItemForOrganoExpedienteDemandado
            // 
            this.ItemForOrganoExpedienteDemandado.Control = this.OrganoExpedienteDemandadoGridControl;
            this.ItemForOrganoExpedienteDemandado.CustomizationFormText = "Demandado";
            this.ItemForOrganoExpedienteDemandado.Location = new System.Drawing.Point(781, 192);
            this.ItemForOrganoExpedienteDemandado.Name = "ItemForOrganoExpedienteDemandado";
            this.ItemForOrganoExpedienteDemandado.Size = new System.Drawing.Size(781, 378);
            this.ItemForOrganoExpedienteDemandado.Text = "Demandado (s)";
            this.ItemForOrganoExpedienteDemandado.TextLocation = DevExpress.Utils.Locations.Top;
            this.ItemForOrganoExpedienteDemandado.TextSize = new System.Drawing.Size(185, 19);
            // 
            // layoutControlItem39
            // 
            this.layoutControlItem39.Control = this.txtMateriaControvertida;
            this.layoutControlItem39.Location = new System.Drawing.Point(0, 160);
            this.layoutControlItem39.Name = "layoutControlItem39";
            this.layoutControlItem39.Size = new System.Drawing.Size(1562, 32);
            this.layoutControlItem39.Text = "Comentarios";
            this.layoutControlItem39.TextSize = new System.Drawing.Size(185, 19);
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.glueContrato;
            this.layoutControlItem6.Location = new System.Drawing.Point(781, 128);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(781, 32);
            this.layoutControlItem6.Text = "Contrato Origen";
            this.layoutControlItem6.TextSize = new System.Drawing.Size(185, 19);
            // 
            // emptySpaceItem6
            // 
            this.emptySpaceItem6.AllowHotTrack = false;
            this.emptySpaceItem6.Location = new System.Drawing.Point(0, 570);
            this.emptySpaceItem6.Name = "emptySpaceItem6";
            this.emptySpaceItem6.Size = new System.Drawing.Size(1562, 413);
            this.emptySpaceItem6.TextSize = new System.Drawing.Size(0, 0);
            // 
            // lcgDatosArbitrajes
            // 
            this.lcgDatosArbitrajes.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.ItemForIdTipoProceso,
            this.layoutControlItem42,
            this.layoutControlItem2,
            this.ItemForObservacion,
            this.layoutControlItem40,
            this.layoutControlItem33,
            this.lycNrocasilla,
            this.ItemForDescripcion,
            this.layoutControlItem29,
            this.emptySpaceItem8});
            this.lcgDatosArbitrajes.Location = new System.Drawing.Point(0, 0);
            this.lcgDatosArbitrajes.Name = "lcgDatosArbitrajes";
            this.lcgDatosArbitrajes.Size = new System.Drawing.Size(1562, 983);
            this.lcgDatosArbitrajes.Text = "DATOS DE ARBITRAJE";
            // 
            // ItemForIdTipoProceso
            // 
            this.ItemForIdTipoProceso.Control = this.IdTipoProcesoGridLookUpEdit;
            this.ItemForIdTipoProceso.CustomizationFormText = "Tipo Proceso";
            this.ItemForIdTipoProceso.Location = new System.Drawing.Point(0, 96);
            this.ItemForIdTipoProceso.Name = "ItemForIdTipoProceso";
            this.ItemForIdTipoProceso.Size = new System.Drawing.Size(1562, 32);
            this.ItemForIdTipoProceso.Text = "Tipo de Arbitraje";
            this.ItemForIdTipoProceso.TextSize = new System.Drawing.Size(185, 19);
            // 
            // layoutControlItem42
            // 
            this.layoutControlItem42.Control = this.glueSecretarioArbitral;
            this.layoutControlItem42.Location = new System.Drawing.Point(0, 128);
            this.layoutControlItem42.Name = "layoutControlItem42";
            this.layoutControlItem42.Size = new System.Drawing.Size(1562, 32);
            this.layoutControlItem42.Text = "Secretario Arbitral ";
            this.layoutControlItem42.TextSize = new System.Drawing.Size(185, 19);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.glueIdSedeArbitral;
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(1562, 32);
            this.layoutControlItem2.Text = "Sede Arbitral";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(185, 19);
            // 
            // ItemForObservacion
            // 
            this.ItemForObservacion.Control = this.ObservacionTextEdit;
            this.ItemForObservacion.CustomizationFormText = "Observacion";
            this.ItemForObservacion.Location = new System.Drawing.Point(0, 192);
            this.ItemForObservacion.Name = "ItemForObservacion";
            this.ItemForObservacion.Size = new System.Drawing.Size(1562, 32);
            this.ItemForObservacion.Text = "Arbitro De Parte (Entidad)";
            this.ItemForObservacion.TextSize = new System.Drawing.Size(185, 19);
            // 
            // layoutControlItem40
            // 
            this.layoutControlItem40.Control = this.glueLaudoFavor;
            this.layoutControlItem40.Location = new System.Drawing.Point(0, 256);
            this.layoutControlItem40.Name = "layoutControlItem40";
            this.layoutControlItem40.Size = new System.Drawing.Size(1562, 32);
            this.layoutControlItem40.Text = "Laudo a Favor de";
            this.layoutControlItem40.TextSize = new System.Drawing.Size(185, 19);
            // 
            // layoutControlItem33
            // 
            this.layoutControlItem33.Control = this.deFechaInstalacionTribunal;
            this.layoutControlItem33.Location = new System.Drawing.Point(0, 32);
            this.layoutControlItem33.Name = "layoutControlItem33";
            this.layoutControlItem33.Size = new System.Drawing.Size(1562, 32);
            this.layoutControlItem33.Text = "Fec. Instalación Tribunal ";
            this.layoutControlItem33.TextSize = new System.Drawing.Size(185, 19);
            // 
            // lycNrocasilla
            // 
            this.lycNrocasilla.Control = this.glueCentroArbitral;
            this.lycNrocasilla.Location = new System.Drawing.Point(0, 64);
            this.lycNrocasilla.Name = "lycNrocasilla";
            this.lycNrocasilla.Size = new System.Drawing.Size(1562, 32);
            this.lycNrocasilla.Text = "Centro Arbitral.";
            this.lycNrocasilla.TextSize = new System.Drawing.Size(185, 19);
            // 
            // ItemForDescripcion
            // 
            this.ItemForDescripcion.Control = this.DescripcionTextEdit;
            this.ItemForDescripcion.CustomizationFormText = "Descripcion";
            this.ItemForDescripcion.Location = new System.Drawing.Point(0, 160);
            this.ItemForDescripcion.Name = "ItemForDescripcion";
            this.ItemForDescripcion.Size = new System.Drawing.Size(1562, 32);
            this.ItemForDescripcion.Text = "Arbitro Unico/Presidente ";
            this.ItemForDescripcion.TextSize = new System.Drawing.Size(185, 19);
            // 
            // layoutControlItem29
            // 
            this.layoutControlItem29.Control = this.txtTercerArbitroAsignado;
            this.layoutControlItem29.Location = new System.Drawing.Point(0, 224);
            this.layoutControlItem29.Name = "layoutControlItem29";
            this.layoutControlItem29.Size = new System.Drawing.Size(1562, 32);
            this.layoutControlItem29.Text = "Arbitro De Parte (Demandante)";
            this.layoutControlItem29.TextAlignMode = DevExpress.XtraLayout.TextAlignModeItem.AutoSize;
            this.layoutControlItem29.TextSize = new System.Drawing.Size(221, 19);
            this.layoutControlItem29.TextToControlDistance = 5;
            // 
            // emptySpaceItem8
            // 
            this.emptySpaceItem8.AllowHotTrack = false;
            this.emptySpaceItem8.Location = new System.Drawing.Point(0, 288);
            this.emptySpaceItem8.Name = "emptySpaceItem8";
            this.emptySpaceItem8.Size = new System.Drawing.Size(1562, 695);
            this.emptySpaceItem8.TextSize = new System.Drawing.Size(0, 0);
            // 
            // splitterItem3
            // 
            this.splitterItem3.AllowHotTrack = true;
            this.splitterItem3.Location = new System.Drawing.Point(0, 1062);
            this.splitterItem3.Name = "splitterItem3";
            this.splitterItem3.Size = new System.Drawing.Size(1596, 9);
            // 
            // bsInstanciaConsulta
            // 
            this.bsInstanciaConsulta.DataSource = typeof(Sistema.Model.viewExpedienteInstancia);
            // 
            // bsInstancia
            // 
            this.bsInstancia.DataSource = typeof(Sistema.Model.ExpedienteInstancia);
            // 
            // bsOrganoJudicialEdit
            // 
            this.bsOrganoJudicialEdit.DataSource = typeof(Sistema.Model.OrganoJudicial);
            // 
            // popMenu
            // 
            this.popMenu.ItemLinks.Add(this.rbtnAddActoProce);
            this.popMenu.ItemLinks.Add(this.rbtnMostrarDatosAdjuntosExp);
            this.popMenu.Name = "popMenu";
            this.popMenu.Ribbon = this.RibbonForm;
            // 
            // rbtnAddActoProce
            // 
            this.rbtnAddActoProce.Caption = "Agregar Acto Procesal";
            this.rbtnAddActoProce.Id = 11;
            this.rbtnAddActoProce.ImageOptions.DisabledImageIndex = 5;
            this.rbtnAddActoProce.ImageOptions.ImageIndex = 5;
            this.rbtnAddActoProce.Name = "rbtnAddActoProce";
            this.rbtnAddActoProce.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.rbtnAddActoProce_ItemClick);
            // 
            // rbtnMostrarDatosAdjuntosExp
            // 
            this.rbtnMostrarDatosAdjuntosExp.Caption = "Mostrar Archivo Adjunto";
            this.rbtnMostrarDatosAdjuntosExp.Id = 14;
            this.rbtnMostrarDatosAdjuntosExp.ImageOptions.ImageIndex = 3;
            this.rbtnMostrarDatosAdjuntosExp.Name = "rbtnMostrarDatosAdjuntosExp";
            this.rbtnMostrarDatosAdjuntosExp.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.rbtnMostrarDatosAdjuntosExp_ItemClick);
            // 
            // popMenuActos
            // 
            this.popMenuActos.ItemLinks.Add(this.rbtnEditarActo);
            this.popMenuActos.ItemLinks.Add(this.rbtnVerAdjuntoActo);
            this.popMenuActos.Name = "popMenuActos";
            this.popMenuActos.Ribbon = this.RibbonForm;
            // 
            // rbtnEditarActo
            // 
            this.rbtnEditarActo.Caption = "Editar Acto Procesal";
            this.rbtnEditarActo.Id = 12;
            this.rbtnEditarActo.ImageOptions.ImageIndex = 5;
            this.rbtnEditarActo.Name = "rbtnEditarActo";
            this.rbtnEditarActo.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.rbtnEditarActo_ItemClick);
            // 
            // rbtnVerAdjuntoActo
            // 
            this.rbtnVerAdjuntoActo.Caption = "Mostrar Archivo Adjunto";
            this.rbtnVerAdjuntoActo.Id = 13;
            this.rbtnVerAdjuntoActo.ImageOptions.ImageIndex = 3;
            this.rbtnVerAdjuntoActo.Name = "rbtnVerAdjuntoActo";
            this.rbtnVerAdjuntoActo.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.rbtnVerAdjuntoActo_ItemClick);
            // 
            // ofdAll
            // 
            this.ofdAll.Filter = "All Files|*.*";
            this.ofdAll.Multiselect = true;
            this.ofdAll.Title = "[ElectroPeru] : Buscar Archivo";
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Magenta;
            this.imageList1.Images.SetKeyName(0, "");
            this.imageList1.Images.SetKeyName(1, "");
            this.imageList1.Images.SetKeyName(2, "");
            this.imageList1.Images.SetKeyName(3, "Search216x16.png");
            this.imageList1.Images.SetKeyName(4, "Attach16x16.png");
            this.imageList1.Images.SetKeyName(5, "Add16x16.png");
            this.imageList1.Images.SetKeyName(6, "ChartLine16x16.png");
            // 
            // rbtnFechaInicio
            // 
            this.rbtnFechaInicio.Caption = "Inicio   ";
            this.rbtnFechaInicio.Edit = this.riFechaInicio;
            this.rbtnFechaInicio.EditWidth = 100;
            this.rbtnFechaInicio.Id = 15;
            this.rbtnFechaInicio.Name = "rbtnFechaInicio";
            this.rbtnFechaInicio.EditValueChanged += new System.EventHandler(this.rbtnFechaInicio_EditValueChanged);
            // 
            // riFechaInicio
            // 
            this.riFechaInicio.AutoHeight = false;
            this.riFechaInicio.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.riFechaInicio.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.riFechaInicio.Name = "riFechaInicio";
            // 
            // rbtnFechaFin
            // 
            this.rbtnFechaFin.Caption = "Fin       ";
            this.rbtnFechaFin.Edit = this.reFechaFin;
            this.rbtnFechaFin.EditWidth = 100;
            this.rbtnFechaFin.Id = 16;
            this.rbtnFechaFin.Name = "rbtnFechaFin";
            this.rbtnFechaFin.EditValueChanged += new System.EventHandler(this.rbtnFechaFin_EditValueChanged);
            // 
            // reFechaFin
            // 
            this.reFechaFin.AutoHeight = false;
            this.reFechaFin.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.reFechaFin.CalendarTimeProperties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.reFechaFin.Name = "reFechaFin";
            // 
            // btnReporte
            // 
            this.btnReporte.Caption = "Reporte de Expediente";
            this.btnReporte.Id = 17;
            this.btnReporte.ImageOptions.LargeImageIndex = 4;
            this.btnReporte.Name = "btnReporte";
            this.btnReporte.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.Large;
            this.btnReporte.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.btnReporte_ItemClick);
            // 
            // printingSystem1
            // 
            this.printingSystem1.Links.AddRange(new object[] {
            this.printableComponentLink1});
            // 
            // printableComponentLink1
            // 
            this.printableComponentLink1.Component = this.gridControl1;
            this.printableComponentLink1.PrintingSystemBase = this.printingSystem1;
            // 
            // bsAbogado
            // 
            this.bsAbogado.DataSource = typeof(Sistema.Model.ExpedienteAsesorLegal);
            // 
            // FExpedienteArbitrales
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1624, 1064);
            this.Controls.Add(this.tpListaEdicion);
            this.Margin = new System.Windows.Forms.Padding(4, 6, 4, 6);
            this.Name = "FExpedienteArbitrales";
            this.Text = "Expedientes Arbitrales";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Controls.SetChildIndex(this.RibbonForm, 0);
            this.Controls.SetChildIndex(this.tpListaEdicion, 0);
            ((System.ComponentModel.ISupportInitialize)(this.RibbonForm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tpListaEdicion)).EndInit();
            this.tpListaEdicion.ResumeLayout(false);
            this.xtraTabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.lcLista)).EndInit();
            this.lcLista.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsActos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribtnShow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsLista)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnMortrarArchivoGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            this.xtraTabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dlcData)).EndInit();
            this.dlcData.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.glueSecretarioArbitral.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsEdicion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtJustificacionContingencia.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.glueLaudoFavor.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMateriaControvertida.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.glueIdSedeArbitral.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMontoReconvencion.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.glueTipoMonedaLaudado.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit3View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMontoLaudado.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.glueTipoMonedaReconvencion.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.glueTipoMonedaCOntrovercia.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTipoDocumento.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LugarGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit3.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLugarProximaAudtextEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsAsesorLegall)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rpiEstudioAbogados)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rpiAbogadoPorEstudio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit3View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsInstanciasExpediente)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemButtonEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.glueTipoContingenciaGridLookUpEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GLueIdSupervisor.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsDocumentos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rpiglueTipoDoc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsTipoDoc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemGridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribtnCargarDOC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rbtnCargarFolder)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribtnShowDOC)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ribtnEliminarFile)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdExpedienteTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CodigoTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.glueUbicacion.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.glueModalidad.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdTipoProcesoGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit2View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deFechaProximaAudidateEdit1.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deFechaProximaAudidateEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deFechaVEncimientoDateEdit1.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deFechaVEncimientoDateEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaInicioDateEdit.Properties.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaInicioDateEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdClaseProcesoGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.glueArchivadoGridLookUpEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdAbogadoPatrocinanteGgridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdAbogadoGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdExpedientePadreGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdNEWIDTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ActoJudicialGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ActoProcesalGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Expediente1GridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Expediente2GridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FechaMovimientoGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridLookUpEdit1View)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NroInstanciaGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ClaseProcesoGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TipoProcesoGridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MontoSolesControversiaTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MontoDolaresTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.OrganoExpedienteDemandanteGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsDemandante)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rpiglueDemandante)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.OrganoExpedienteDemandadoGridControl)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsDemandado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rpiglueDemandado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deHoraProximaAudiDateEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.glueCentroArbitral.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.EstadoActualgridLookUpEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.glueContrato.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IdNotificacionGgridLookUpEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DescripcionTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ObservacionTextEdit.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTercerArbitroAsignado.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.deFechaInstalacionTribunal.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIdExpediente)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIdExpedientePadre)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIdNEWID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForActoJudicial)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForActoProcesal)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForExpediente1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForExpediente2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFechaMovimiento)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForNroInstancia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForClaseProceso)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForTipoProceso)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgTabsEdicion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgEstadoArbitral)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIdAbogado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForMontoDolares)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForMontoSoles)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tabbedControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgDatosGenerales)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForCodigo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForFechaInicio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIdClaseProceso)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOrganoExpedienteDemandante)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForOrganoExpedienteDemandado)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgDatosArbitrajes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForIdTipoProceso)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForObservacion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lycNrocasilla)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ItemForDescripcion)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitterItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsInstanciaConsulta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsInstancia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsOrganoJudicialEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsJuzgados)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popMenuActos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.riFechaInicio.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.riFechaInicio)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reFechaFin.CalendarTimeProperties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.reFechaFin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.printingSystem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsAbogado)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private DevExpress.XtraTab.XtraTabControl tpListaEdicion;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private ExtLayoutControl dlcData;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControl lcLista;
        private DevExpress.XtraGrid.GridControl gridControl2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private System.Windows.Forms.BindingSource bsLista;
        private System.Windows.Forms.BindingSource bsActos;
        private DevExpress.XtraGrid.Columns.GridColumn colIdActoPro;
        private DevExpress.XtraGrid.Columns.GridColumn colIdExpediente1;
        private DevExpress.XtraGrid.Columns.GridColumn colContenido;
        private DevExpress.XtraGrid.Columns.GridColumn colContenido1;
        private DevExpress.XtraGrid.Columns.GridColumn colIdNEWID1;
        private DevExpress.XtraGrid.Columns.GridColumn colIdExpedienteInstancia;
        private DevExpress.XtraGrid.Columns.GridColumn colExpediente;
        private DevExpress.XtraGrid.Columns.GridColumn colActoProcesalContenido;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private System.Windows.Forms.BindingSource bsEdicion;
        private DevExpress.XtraEditors.TextEdit IdExpedienteTextEdit;
        private DevExpress.XtraEditors.TextEdit CodigoTextEdit;
        private DevExpress.XtraEditors.GridLookUpEdit IdTipoProcesoGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit2View;
        private DevExpress.XtraEditors.DateEdit FechaInicioDateEdit;
        private DevExpress.XtraEditors.GridLookUpEdit IdClaseProcesoGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView5;
        private DevExpress.XtraEditors.GridLookUpEdit IdAbogadoGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView6;
        private DevExpress.XtraEditors.GridLookUpEdit IdExpedientePadreGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView7;
        private DevExpress.XtraEditors.TextEdit IdNEWIDTextEdit;
        private DevExpress.XtraEditors.GridLookUpEdit ActoJudicialGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView9;
        private DevExpress.XtraEditors.GridLookUpEdit ActoProcesalGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView10;
        private DevExpress.XtraEditors.GridLookUpEdit Expediente1GridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView12;
        private DevExpress.XtraEditors.GridLookUpEdit Expediente2GridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView13;
        private DevExpress.XtraLayout.LayoutControlItem ItemForIdExpediente;
        private DevExpress.XtraLayout.LayoutControlItem ItemForIdExpedientePadre;
        private DevExpress.XtraLayout.LayoutControlItem ItemForIdNEWID;
        private DevExpress.XtraLayout.LayoutControlItem ItemForActoJudicial;
        private DevExpress.XtraLayout.LayoutControlItem ItemForActoProcesal;
        private DevExpress.XtraLayout.LayoutControlItem ItemForExpediente1;
        private DevExpress.XtraLayout.LayoutControlItem ItemForExpediente2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem ItemForDescripcion;
        private DevExpress.XtraLayout.LayoutControlItem ItemForObservacion;
        private DevExpress.XtraLayout.LayoutControlItem ItemForIdAbogado;
        private DevExpress.XtraLayout.LayoutControlItem ItemForIdTipoProceso;
        private DevExpress.XtraBars.BarButtonItem rbtnAddActoProce;
        private DevExpress.XtraBars.PopupMenu popMenu;
        private DevExpress.XtraEditors.SimpleButton btnAddE;
        private DevExpress.XtraEditors.SimpleButton btnAddActoProcesal;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraGrid.Columns.GridColumn colCodigo;
        private DevExpress.XtraGrid.Columns.GridColumn colFechaInicio;
        private DevExpress.XtraLayout.SplitterItem splitterItem1;
        private DevExpress.XtraLayout.SplitterItem splitterItem2;
        private System.Windows.Forms.BindingSource bsInstancia;
        private DevExpress.XtraEditors.GridLookUpEdit FechaMovimientoGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit1View;
        private DevExpress.XtraEditors.GridLookUpEdit NroInstanciaGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView8;
        private DevExpress.XtraEditors.GridLookUpEdit ClaseProcesoGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView16;
        private DevExpress.XtraEditors.GridLookUpEdit TipoProcesoGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView17;
        private DevExpress.XtraLayout.LayoutControlItem ItemForFechaMovimiento;
        private DevExpress.XtraLayout.LayoutControlItem ItemForNroInstancia;
        private DevExpress.XtraLayout.LayoutControlItem ItemForClaseProceso;
        private DevExpress.XtraLayout.LayoutControlItem ItemForTipoProceso;
        private System.Windows.Forms.BindingSource bsInstanciaConsulta;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit ribtnShow;
        private System.Windows.Forms.ImageList imgList;
        public DevExpress.XtraGrid.Columns.GridColumn colMostrar;
        private DevExpress.XtraEditors.SimpleButton btnMostrarArchivo;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn5;
        private System.Windows.Forms.BindingSource bsJuzgados;
        private DevExpress.XtraBars.PopupMenu popMenuActos;
        private DevExpress.XtraBars.BarButtonItem rbtnEditarActo;
        private DevExpress.XtraBars.BarButtonItem rbtnVerAdjuntoActo;
        private DevExpress.XtraLayout.LayoutControlItem ItemForMontoDolares;
        private DevExpress.XtraEditors.TextEdit MontoSolesControversiaTextEdit;
        private DevExpress.XtraEditors.TextEdit MontoDolaresTextEdit;
        private DevExpress.XtraGrid.Columns.GridColumn colMontoSoles;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private System.Windows.Forms.BindingSource bsOrganoJudicialEdit;
        private DevExpress.XtraBars.BarButtonItem rbtnMostrarDatosAdjuntosExp;
        internal System.Windows.Forms.OpenFileDialog ofdAll;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit btnMortrarArchivoGrid;
        private System.Windows.Forms.ImageList imageList1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.GridControl OrganoExpedienteDemandanteGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView18;
        private DevExpress.XtraGrid.GridControl OrganoExpedienteDemandadoGridControl;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView19;
        private DevExpress.XtraLayout.LayoutControlItem ItemForOrganoExpedienteDemandante;
        private DevExpress.XtraLayout.LayoutControlItem ItemForOrganoExpedienteDemandado;
        private System.Windows.Forms.BindingSource bsDemandante;
        private DevExpress.XtraGrid.Columns.GridColumn colIdDemandante1;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit rpiglueDemandante;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView20;
        private System.Windows.Forms.BindingSource bsDemandado;
        private DevExpress.XtraGrid.Columns.GridColumn colIdDemandado1;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit rpiglueDemandado;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView21;
        private DevExpress.XtraGrid.GridControl gridControl4;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private System.Windows.Forms.BindingSource bsDocumentos;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit ribtnCargarDOC;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit ribtnShowDOC;
        private DevExpress.XtraGrid.Columns.GridColumn colIdDocumento;
        private DevExpress.XtraGrid.Columns.GridColumn colNombre;
        private DevExpress.XtraGrid.Columns.GridColumn colExtension;
        private DevExpress.XtraGrid.Columns.GridColumn colIdNEWID3;
        private DevExpress.XtraGrid.Columns.GridColumn colDocumento1;
        public DevExpress.XtraGrid.Columns.GridColumn gridColumnCargarDoc;
        public DevExpress.XtraGrid.Columns.GridColumn gridColumnMostrarDOC;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraBars.BarEditItem rbtnFechaInicio;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit riFechaInicio;
        private DevExpress.XtraBars.BarEditItem rbtnFechaFin;
        private DevExpress.XtraEditors.Repository.RepositoryItemDateEdit reFechaFin;
        private DevExpress.XtraBars.BarButtonItem btnReporte;
        private DevExpress.XtraGrid.Columns.GridColumn colNDemandados;
        private DevExpress.XtraGrid.Columns.GridColumn colNDemandantes;
        private DevExpress.XtraPrinting.PrintingSystem printingSystem1;
        private DevExpress.XtraPrinting.PrintableComponentLink printableComponentLink1;
        private DevExpress.XtraEditors.GridLookUpEdit GLueIdSupervisor;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private System.Windows.Forms.FolderBrowserDialog folderDialog1;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit rbtnCargarFolder;
        public DevExpress.XtraGrid.Columns.GridColumn colGargarFolder;
        private DevExpress.XtraGrid.GridControl gridControl5;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView25;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn10;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn11;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn12;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn13;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn15;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn16;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn17;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn18;
        public DevExpress.XtraGrid.Columns.GridColumn gridColumn19;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn20;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn21;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn22;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn23;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem11;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup7;
        private DevExpress.XtraEditors.GridLookUpEdit IdAbogadoPatrocinanteGgridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView26;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraGrid.GridControl gridControl6;
        private System.Windows.Forms.BindingSource bsInstanciasExpediente;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView27;
        private DevExpress.XtraGrid.Columns.GridColumn colIdInstancia2;
        private DevExpress.XtraGrid.Columns.GridColumn colIdOrgano2;
        private DevExpress.XtraGrid.Columns.GridColumn colFechaInicio3;
        private DevExpress.XtraGrid.Columns.GridColumn colFechaFinal2;
        private DevExpress.XtraGrid.Columns.GridColumn colAutomisorio2;
        private DevExpress.XtraGrid.Columns.GridColumn colTasacionInstancia1;
        private DevExpress.XtraGrid.Columns.GridColumn colApelacion2;
        private DevExpress.XtraGrid.Columns.GridColumn colObservacion3;
        private DevExpress.XtraGrid.Columns.GridColumn colComentarios2;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit repositoryItemButtonEdit2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem13;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem14;
        private DevExpress.XtraEditors.SimpleButton btnEditInstancia;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraEditors.SimpleButton btnEditarActo;
        private DevExpress.XtraEditors.SimpleButton btnAddActo;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private DevExpress.XtraGrid.Columns.GridColumn colInstnacia;
        private DevExpress.XtraGrid.Columns.GridColumn colInstancia;
        private DevExpress.XtraLayout.TabbedControlGroup tabbedControlGroup2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup8;
        private DevExpress.XtraGrid.GridControl gridControl7;
        private System.Windows.Forms.BindingSource bsAsesorLegall;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView11;
        private DevExpress.XtraGrid.Columns.GridColumn colID;
        private DevExpress.XtraGrid.Columns.GridColumn colIdAsesorLegalEstudio;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit rpiEstudioAbogados;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView23;
        private DevExpress.XtraGrid.Columns.GridColumn colIdAsesorLegalAbogado;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit rpiAbogadoPorEstudio;
        private DevExpress.XtraGrid.Views.Grid.GridView repositoryItemGridLookUpEdit3View;
        private DevExpress.XtraGrid.Columns.GridColumn colFechaInicio1;
        private DevExpress.XtraGrid.Columns.GridColumn colFechaFIn;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private DevExpress.XtraEditors.GridLookUpEdit glueArchivadoGridLookUpEdit1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView28;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
        private DevExpress.XtraEditors.GridLookUpEdit glueTipoContingenciaGridLookUpEdit1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView29;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem20;
        private DevExpress.XtraEditors.TextEdit txtLugarProximaAudtextEdit1;
        private DevExpress.XtraEditors.DateEdit deFechaProximaAudidateEdit1;
        private DevExpress.XtraEditors.DateEdit deFechaVEncimientoDateEdit1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem24;
        private DevExpress.XtraLayout.LayoutControlItem lycNrocasilla;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem25;
        private DevExpress.XtraEditors.TextEdit textEdit3;
        private DevExpress.XtraEditors.TextEdit textEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn colInstanciaTexto;
        private DevExpress.XtraGrid.Columns.GridColumn colOrganoTexto;
        private DevExpress.XtraEditors.TimeEdit deHoraProximaAudiDateEdit2;
        private DevExpress.XtraEditors.GridLookUpEdit LugarGridLookUpEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView31;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem21;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem22;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem23;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem27;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem26;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem28;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraGrid.Columns.GridColumn colArchivado;
        private DevExpress.XtraGrid.Columns.GridColumn colArchivadoSIoNO;
        private DevExpress.XtraEditors.GridLookUpEdit glueCentroArbitral;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView32;
        private DevExpress.XtraEditors.TextEdit EstadoActualgridLookUpEdit1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem29;
        private DevExpress.XtraEditors.TextEdit glueContrato;
        private DevExpress.XtraGrid.Columns.GridColumn colNroOS_RA_C;
        private DevExpress.XtraEditors.TextEdit IdNotificacionGgridLookUpEdit;
        private DevExpress.XtraEditors.TextEdit txtTipoDocumento;
        private DevExpress.XtraEditors.GridLookUpEdit glueUbicacion;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView24;
        private DevExpress.XtraEditors.GridLookUpEdit glueModalidad;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView22;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem31;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem32;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem33;
        private DevExpress.XtraEditors.TextEdit txtMontoReconvencion;
        private DevExpress.XtraEditors.GridLookUpEdit glueTipoMonedaLaudado;
        private DevExpress.XtraGrid.Views.Grid.GridView gridLookUpEdit3View;
        private DevExpress.XtraEditors.TextEdit txtMontoLaudado;
        private DevExpress.XtraEditors.GridLookUpEdit glueTipoMonedaReconvencion;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView33;
        private DevExpress.XtraEditors.GridLookUpEdit glueTipoMonedaCOntrovercia;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView30;
        private DevExpress.XtraEditors.TextEdit txtJustificacionContingencia;
        private DevExpress.XtraEditors.GridLookUpEdit glueLaudoFavor;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView15;
        private DevExpress.XtraEditors.TextEdit txtMateriaControvertida;
        private DevExpress.XtraEditors.GridLookUpEdit glueIdSedeArbitral;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView14;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem39;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem40;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem41;
        private DevExpress.XtraEditors.GridLookUpEdit glueSecretarioArbitral;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView34;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem42;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraEditors.Repository.RepositoryItemGridLookUpEdit rpiglueTipoDoc;
        private DevExpress.XtraGrid.Views.Grid.GridView repositoryItemGridLookUpEdit1View;
        private System.Windows.Forms.BindingSource bsTipoDoc;
        private DevExpress.XtraGrid.Columns.GridColumn colDescripcion1;
        private DevExpress.XtraEditors.GridLookUpEdit DescripcionTextEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView35;
        private DevExpress.XtraEditors.GridLookUpEdit ObservacionTextEdit;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView36;
        private DevExpress.XtraEditors.GridLookUpEdit txtTercerArbitroAsignado;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView37;
        private DevExpress.XtraLayout.TabbedControlGroup lcgTabsEdicion;
        private DevExpress.XtraLayout.LayoutControlGroup lcgDatosArbitrajes;
        private DevExpress.XtraLayout.LayoutControlGroup lcgDatosGenerales;
        private DevExpress.XtraLayout.LayoutControlItem ItemForCodigo;
        private DevExpress.XtraLayout.LayoutControlItem ItemForFechaInicio;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem30;
        private DevExpress.XtraLayout.LayoutControlItem ItemForIdClaseProceso;
        private DevExpress.XtraGrid.Columns.GridColumn colEliminar;
        private DevExpress.XtraEditors.Repository.RepositoryItemButtonEdit ribtnEliminarFile;
        private DevExpress.XtraGrid.Columns.GridColumn colMateriaArbitral;
        private DevExpress.XtraGrid.Columns.GridColumn colModalidad;
        private DevExpress.XtraGrid.Columns.GridColumn colSedeArbitral;
        private DevExpress.XtraGrid.Columns.GridColumn colDescripcionTipoArbitraje;
        private DevExpress.XtraGrid.Columns.GridColumn colTipoMonedaControversia;
        private DevExpress.XtraGrid.Columns.GridColumn colNombreAsesorLegalPatrocinador;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem4;
        private DevExpress.XtraLayout.LayoutControlGroup lcgEstadoArbitral;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup10;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem34;
        private DevExpress.XtraLayout.LayoutControlItem ItemForMontoSoles;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup11;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem35;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem38;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup12;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem37;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem36;
        private DevExpress.XtraEditors.TextEdit deFechaInstalacionTribunal;
        private DevExpress.XtraLayout.SplitterItem splitterItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem8;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem7;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem5;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem6;
        private System.Windows.Forms.BindingSource bsAbogado;
    }
}